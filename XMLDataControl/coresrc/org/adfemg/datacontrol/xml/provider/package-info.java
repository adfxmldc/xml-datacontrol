/**
 * Providers are plug-ins to the XML DataControl that provide certain parts of its functionality. Having this highly
 * pluggable framework with independend providers allows a user to replace or enhance certain parts of the XML
 * DataControl.
 */
package org.adfemg.datacontrol.xml.provider;


package org.adfemg.datacontrol.xml.provider.data;

import oracle.adf.model.adapter.AdapterException;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Element;

/**
 * DataProvider that simply creates a new XML element that can be used as
 * a root element for creating a new XML tree. For example, this data
 * provider can be used to instantiate an Department element. The user can
 * than add a Manager, Employees, Location, etc through the normal datacontrol
 * collections. Once the Department (and all its children) are complete they
 * can be used as the argument to another DataControl that invokes the
 * web service to save this department using the WSDataProvider.
 * <p>
 * This provider doesn't need any additional configuration, so this is all
 * you need in DataControls.dcx:
 * {@code <data-provider class="org.adfemg.datacontrol.xml.provider.data.EmptyElementProvider"/>}
 */
public class EmptyElementProvider extends ProviderImpl implements DataProvider {

    /**
     * Default no-argument constructor.
     */
    public EmptyElementProvider() {
        super();
    }

    @Override
    public Element getRootElement(final DataRequest request) {
        try {
            final XMLSchema schema = request.getSchema();
            return XmlFactory.newDocumentElement(schema.getTargetNS(), request.getRootNodeName());
        } catch (XSDException e) {
            throw new AdapterException(e);
        }
    }

}

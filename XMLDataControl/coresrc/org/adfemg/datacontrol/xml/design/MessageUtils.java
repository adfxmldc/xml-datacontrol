package org.adfemg.datacontrol.xml.design;

import oracle.ide.Ide;

import oracle.javatools.dialogs.MessageDialog;


/**
 * Simplified wrapper for oracle.javatools.dialogs.MessageDialog that only
 * exposes method we actually need. Without this utility class the caller
 * might want to invoke any of the static methods in oracle.javatools.dialogs.MessageDialog
 * directly. Downside to that is that these need a parent component for which the
 * caller would need to invoke oracle.ide.Ide#getMainWindow. This introduces
 * a dependency on the oracle.ide.Ide class which might not be available at
 * runtime, for example when running the datacontrol tester, ojdeploy or ojaudit.
 * With this MessageUtils class the caller can first check for
 * DesignTimeUtils.isGraphicalDesignTime() which is always available and then only
 * call one of our methods when running in graphical design time and the Ide class
 * is available. When not running in graphical design-time the caller can prevent
 * calling our class and thus remove the dependency on oracle.ide.Ide
 * <p><b>Warning:</b> this class introduces a dependency on JDeveloper design time classes.
 * Be sure to check DesignTimeUtils.isGraphicalDesignTime() before invoking anything from
 * this class to prevent ClassNotFoundException's on design time classes or
 * java.awt.HeadlessException.
 */
public final class MessageUtils {

    /**
     * Private constructor to prevent instances being created.
     */
    private MessageUtils() {
    }

    /**
     * Show an error message dialog. Be sure to check DesignTimeUtils.isGraphicalDesignTime() before invoking
     * this method to prevent errors about non existing GUI's or reliancy on JDeveloper design time classes at
     * runtime
     * @param title title of the dialog to show
     * @param errorMessage message to show in the error dialog
     * @see DesignTimeUtils#isGraphicalDesignTime
     */
    public static void showErrorMessage(String title, String errorMessage) {
        MessageDialog.error(Ide.getMainWindow(), errorMessage, title, null);
    }

    /**
     * Show an info message dialog. Be sure to check DesignTimeUtils.isGraphicalDesignTime() before invoking
     * this method to prevent errors about non existing GUI's or reliancy on JDeveloper design time classes at
     * runtime
     * @param title title of the dialog to show
     * @param infoMessage message to show in the info dialog
     * @see DesignTimeUtils#isGraphicalDesignTime
     */
    public static void showInformationMessage(String title, String infoMessage) {
        MessageDialog.information(Ide.getMainWindow(), infoMessage, title, null);
    }

}

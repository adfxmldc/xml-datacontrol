package org.adfemg.datacontrol.xml.provider.customization;

import oracle.adf.model.adapter.dataformat.MethodDef;

import org.adfemg.datacontrol.xml.provider.Provider;


/**
 * The provider of the customization class for the customization off a datacontrol.
 */
public interface CustomizationProvider extends Provider {

    /**
     * Perform any necessary customization on the return structure of a datacontrol operation and all of its
     * nested structures.
     * @param method the datacontrol operation (and all of its children) to be customized
     */
    void customize(MethodDef method);

}

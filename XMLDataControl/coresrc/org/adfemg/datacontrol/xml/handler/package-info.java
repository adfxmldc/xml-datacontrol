/**
 * Handlers are hooks that can be registered with the runtime classes and get invoked before, after or instead of
 * certain functions in the runtime part of the XML DataControl. Is mostly used to implement the java annotation
 * framework for customizing the XML DataControl.
 */
package org.adfemg.datacontrol.xml.handler;


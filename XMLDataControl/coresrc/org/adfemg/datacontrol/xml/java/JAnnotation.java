package org.adfemg.datacontrol.xml.java;


/**
 * Represents a single Java annotation on a class, method, declaration, etc.
 */
public interface JAnnotation {

    /**
     * Gets the value of an annotation element (aka as attribute). For example <tt>bar</tt> when asking
     * for element <tt>t2</tt> on an annotation like <tt>@MyAnnotation(t1="foo", t2="bar")</tt>
     * @param name name pf the annotation element or attribute to get
     * @return value of the element, which might be the default als defined on the annotation or <tt>null</tt>
     * of not set
     */
    public Object getElement(final String name);

}

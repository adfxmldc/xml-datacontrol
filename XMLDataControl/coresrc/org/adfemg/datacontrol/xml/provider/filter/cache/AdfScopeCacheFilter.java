package org.adfemg.datacontrol.xml.provider.filter.cache;

import java.util.Collections;
import java.util.Map;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.filter.CacheFilter;

import org.w3c.dom.Element;

public abstract class AdfScopeCacheFilter extends CacheFilter {

    public AdfScopeCacheFilter(DataProvider dataProvider) {
        super(dataProvider);
    }

    protected abstract Map<String, Object> getScope();

    protected Map<Map, Element> createCache() {
        return Collections.synchronizedMap(new org.apache.commons.collections.map.LRUMap());
    }

    @Override
    protected Map<Map, Element> getCache(String structDefFullName) {
        Map<String, Object> scope = getScope();
        String scopeKey = getScopeKey(structDefFullName);
        if (!scope.containsKey(scopeKey)) {
            scope.put(scopeKey, createCache());
        }
        return (Map<Map, Element>) scope.get(scopeKey);
    }

    protected String getScopeKey(String structDefFullName) {
        return this.getClass().getName() + "." + structDefFullName + ".CacheStore";
    }

}

package org.adfemg.datacontrol.view.uitest;

import com.redheap.selenium.junit.SavePageSourceOnFailure;
import com.redheap.selenium.junit.ScreenshotOnFailure;
import com.redheap.selenium.junit.WebDriverResource;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.adfemg.datacontrol.view.uitest.PerceptualDiffMatcher.*;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;


public class EmployeeTest {

    @ClassRule
    public static WebDriverResource driver = new WebDriverResource();

    @Rule
    public TestFragmentProvider<EmployeePageFragment> tester =
        new TestFragmentProvider<>(EmployeePageFragment.class, "http://127.0.0.1:7101/xmldc", driver.getDriver());

    private static final Path outputDir = Paths.get("coverage");
    private static final JacocoReporter reporter =
        JacocoReporter.builder().withClasses(Paths.get("classes")).withSrc(Paths.get("src")).withOutputDir(outputDir).build();
    @ClassRule
    public static JacocoRule jacoco = JacocoRule.builder().withOutputDir(outputDir).withReporter(reporter).build();

    @Rule
    public ScreenshotOnFailure sof = new ScreenshotOnFailure(driver.getDriver());
    @Rule
    public SavePageSourceOnFailure spsof = new SavePageSourceOnFailure(driver.getDriver());

    @Test
    public void fullTest() throws Exception {
        EmployeePageFragment fragment = tester.selectFromTree("hr-emp-tf", "simpletest");
        // assertions on intitial data
        assertThat(fragment.getId().getValue(), hasToString("114"));
        assertThat(fragment.getFirstName().getValue(), hasToString("Den"));
        assertThat(fragment.getLastName().getValue(), hasToString("Raphaely"));
        assertThat(fragment.getEmail().getValue(), hasToString("DRAPHEAL"));
        assertThat(fragment.getPhoneNumber().getValue(), hasToString("515.127.4561"));
        assertThat(fragment.getHireDate().getValue(), hasToString("2002-12-07T00:00:00.000Z"));
        assertThat(fragment.getSalary().getValue(), hasToString("11000"));
        assertThat(fragment.getCommissionPercentage().getValue(), nullValue());

        // job info should not be shown by default
        assertThat(fragment.getShowJobInfo().getValue(), hasToString("false"));
        assertThat(fragment.getJobId(), nullValue());
        assertThat(fragment.getJobTitle(), nullValue());
        assertThat(fragment.getJobMinSalary(), nullValue());
        assertThat(fragment.getJobMaxSalary(), nullValue());
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("01-initial-data.png")));

        // job info should be shown after checking checkbox
        fragment.getShowJobInfo().click();
        assertThat(fragment.getJobId().getValue(), hasToString("PU_MAN"));
        assertThat(fragment.getJobTitle().getValue(), hasToString("Purchasing Manager"));
        assertThat(fragment.getJobMinSalary().getValue(), hasToString("8000"));
        assertThat(fragment.getJobMaxSalary().getValue(), hasToString("15000"));
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("02-with-job-info.png")));

        // job info should disappear when unchecking checkbox
        fragment.getShowJobInfo().click();
        assertThat(fragment.getJobId(), nullValue());

        // assert initial state of navigation buttons
        assertThat(fragment.getFirstButton().isDisabled(), is(true));
        assertThat(fragment.getPreviousButton().isDisabled(), is(true));
        assertThat(fragment.getNextButton().isDisabled(), is(false));
        assertThat(fragment.getLastButton().isDisabled(), is(false));
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("03-closed-job-info.png")));

        // navigate to other record
        fragment.getNextButton().click();
        assertThat(fragment.getFirstButton().isDisabled(), is(false));
        assertThat(fragment.getPreviousButton().isDisabled(), is(false));
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("04-next-row.png")));

        // navigate back to Den Raphaely and try to lower salary
        fragment.getPreviousButton().click();
        Long salary = (Long) fragment.getSalary().getValue();
        //resetJacoco();
        fragment.getSalary().typeValue(String.valueOf(salary.longValue() + 1));
        fragment.getSubmitButton().click();
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("05-higher-salary.png")));
        fragment.getSalary().typeValue(String.valueOf(salary.longValue() - 1));
        assertTrue(fragment.getSalary().isValid());
        // submit form to raise validation exception
        fragment.getSubmitButton().click();
        assertFalse(fragment.getSalary().isValid());
        assertTrue(fragment.getSalary().hasMessage("Salary can not be lowered"));
        assertThat(driver.getDriver(), hasSimilarScreenshot(Paths.get("06-lower-salary.png")));

    }

    @Test
    public void simpleTest() throws Exception {
        EmployeePageFragment fragment = tester.selectFromTree("hr-emp-tf", "simpletest");
        // assertions on intitial data
        assertThat(fragment.getId().getValue(), hasToString("114"));
        assertThat(fragment.getFirstName().getValue(), hasToString("Den"));
        assertThat(fragment.getLastName().getValue(), hasToString("Raphaely"));
        assertThat(fragment.getEmail().getValue(), hasToString("DRAPHEAL"));
        assertThat(fragment.getPhoneNumber().getValue(), hasToString("515.127.4561"));
        assertThat(fragment.getHireDate().getValue(), hasToString("2002-12-07T00:00:00.000Z"));
        assertThat(fragment.getSalary().getValue(), hasToString("11000"));
        assertThat(fragment.getCommissionPercentage().getValue(), nullValue());

        // job info should not be shown by default
        assertThat(fragment.getShowJobInfo().getValue(), hasToString("false"));
        assertThat(fragment.getJobId(), nullValue());
        assertThat(fragment.getJobTitle(), nullValue());
        assertThat(fragment.getJobMinSalary(), nullValue());
        assertThat(fragment.getJobMaxSalary(), nullValue());
    }

    public static void main(String[] args) {
        org.junit.runner.JUnitCore.main(new String[] { EmployeeTest.class.getName() });
    }
}

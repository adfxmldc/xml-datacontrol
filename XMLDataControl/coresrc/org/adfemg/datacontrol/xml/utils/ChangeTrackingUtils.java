package org.adfemg.datacontrol.xml.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;


/**
 * Utility methods for Change Tracking.
 * Adds processing instructions to elements or element attributes which are modified
 * deleted or inserted.
 */
public class ChangeTrackingUtils {

    /** Added as processing instruction to elements which are modified. */
    public static final String PI_CHANGE_VALUE = "change-value";

    /** Added as processing instruction to element attributes which are modified. */
    public static final String PI_CHANGE_ATTRIBUTE = "change-attribute-value";

    /** Added as processing instruction to an element collection which is deleted. */
    public static final String PI_CHANGE_DELETED = "change-deleted";

    /** Added as processing instruction to an element collection which is inserted. */
    public static final String PI_CHANGE_INSERTED = "change-inserted";

    public static final String CHANGE_DATA_OLD = "old";
    public static final String CHANGE_DATA_NAME = "name";

    private static final Pattern OLD_VALUE_ATTR_REGEX =
        Pattern.compile("\\s*" + CHANGE_DATA_OLD + "=\"(.*)\"\\s+" + CHANGE_DATA_NAME + "=\".*\"\\s*");
    private static final Pattern OLD_VALUE_ELEM_REGEX = Pattern.compile("\\s*" + CHANGE_DATA_OLD + "=\"(.*)\"\\s*");

    /**
     * Private constructor to prevent instances being created.
     */
    private ChangeTrackingUtils() {
        super();
    }

    /**
     * Method to retain existing value of an xml element (simplecontent) or xml attribute before changing its value.
     * Will add a {@value #PI_CHANGE_VALUE} processing instruction when changing an element or a
     * {@value #PI_CHANGE_ATTRIBUTE} processing instructions to the owning element when changing an attribute.
     * @param node org.w3c.dom.Element or org.w3c.dom.Attribute that is about to be changed
     */
    public static void markChanging(final Node node) {
        if (Node.ELEMENT_NODE == node.getNodeType()) {
            final Element element = (Element) node;
            final ProcessingInstruction pi = addProcessingInstructionIfNotExists(element, PI_CHANGE_VALUE);
            if (pi != null) {
                // processing instruction was added to the DOM
                pi.setData(buildProcessingInstructionData(CHANGE_DATA_OLD, element.getTextContent()));
            }
        } else if (Node.ATTRIBUTE_NODE == node.getNodeType()) {
            final Attr attr = (Attr) node;
            ProcessingInstruction pi =
                addNamedProcessingInstructionIfNotExists(attr.getOwnerElement(), PI_CHANGE_ATTRIBUTE, attr.getName());
            if (pi != null) {
                // processing instruction was added to the DOM
                pi.setData(buildProcessingInstructionData(CHANGE_DATA_OLD, attr.getValue()) + " " + pi.getData());
            }
        } else {
            throw new IllegalArgumentException("unsupported node type " + node);
        }
    }

    /**
     * Method to mark a new xml element (simplecontent) or xml attribute that previously did not exist in the DOM.
     * Will add a {@value #PI_CHANGE_VALUE} processing instruction to the new element or {@value #PI_CHANGE_ATTRIBUTE}
     * to the owning element when marking an attribute.
     * @param node org.w3c.dom.Element or org.w3c.dom.Attribute that has been added to the DOM
     */
    public static void markNewTextContent(final Node node) {
        if (Node.ELEMENT_NODE == node.getNodeType()) {
            final ProcessingInstruction pi = addProcessingInstructionIfNotExists((Element) node, PI_CHANGE_VALUE);
            if (pi != null) {
                // processing instruction was added to the DOM
                pi.setData(buildProcessingInstructionData(CHANGE_DATA_OLD, ""));
            }
        } else if (Node.ATTRIBUTE_NODE == node.getNodeType()) {
            final Attr attr = (Attr) node;
            ProcessingInstruction pi =
                addNamedProcessingInstructionIfNotExists(attr.getOwnerElement(), PI_CHANGE_ATTRIBUTE, attr.getName());
            if (pi != null) {
                // processing instruction was added to the DOM
                pi.setData(buildProcessingInstructionData(CHANGE_DATA_OLD, "") + " " + pi.getData());
            }
        } else {
            throw new IllegalArgumentException("unsupported node type " + node);
        }
    }

    /**
     * Checks if the given node has the {@value #PI_CHANGE_VALUE} or {@value #PI_CHANGE_ATTRIBUTE} processing
     * instruction. When invoked with a org.w3c.dom.Element this will look for a {@value #PI_CHANGE_VALUE}
     * instruction on the element itself. When invoked with a org.w3c.dom.Attr it will look for a
     * {@value #PI_CHANGE_ATTRIBUTE} for the given attribute on the parent element of the attribute.
     * In short it means this will return true if {@link #markChanged} or {@link #markNewTextContent} has been
     * invoked on the same node.
     * @param node the element or attribute being checked for the processing instruction
     * @return {@code true} if the given element or attribute has the processing instruction to mark it as changed
     * @throws IllegalArgumentException when given node is not an Element or Attr
     */
    public static boolean isChanged(final Node node) {
        switch (node.getNodeType()) {
        case Node.ATTRIBUTE_NODE:
            Attr attr = (Attr) node;
            return !findNamedProcessingInstructions(attr.getOwnerElement(), PI_CHANGE_ATTRIBUTE,
                                                    attr.getName()).isEmpty();
        case Node.ELEMENT_NODE:
            return !DomUtils.findChildProcessingInstructions((Element) node, PI_CHANGE_VALUE).isEmpty();
        default:
            throw new IllegalArgumentException("unsupported node type " + node);
        }
    }

    /**
     * Checks if the given xml element value or xml attribute has been changed previously and thus has the
     * associated processing instruction and returns the old value for the element/attribute that is stored in that
     * processing instruction.
     * @param node org.w3c.dom.Element with simple content or org.w3c.dom.Attr
     * @return old value of the element or attribute which could be an empty string if there was no initial value
     * before the change. This returns {@code null} if the element/attribute has no processing instruction and thus
     * was never changed.
     */
    public static String getOldValue(final Node node) {
        if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
            Attr attr = (Attr) node;
            List<ProcessingInstruction> pis =
                findNamedProcessingInstructions(attr.getOwnerElement(), PI_CHANGE_ATTRIBUTE, attr.getName());
            if (pis.isEmpty()) {
                return null; // no processing instruction
            }
            String pidata = pis.get(0).getData();
            Matcher matcher = OLD_VALUE_ATTR_REGEX.matcher(pidata);
            if (!matcher.matches()) {
                throw new IllegalStateException("invalid attribute processing instruction data: " + pidata);
            }
            return matcher.group(1);
        } else if (node.getNodeType() == Node.ELEMENT_NODE) {
            List<ProcessingInstruction> pis =
                DomUtils.findChildProcessingInstructions((Element) node, PI_CHANGE_VALUE);
            if (pis.isEmpty()) {
                return null; // no processing instruction
            }
            String pidata = pis.get(0).getData();
            Matcher matcher = OLD_VALUE_ELEM_REGEX.matcher(pidata);
            if (!matcher.matches()) {
                throw new IllegalStateException("invalid element processing instruction data: " + pidata);
            }
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException("unsupported node type " + node);
        }
    }

    /**
     * Adds the {@value #PI_CHANGE_DELETED} processing instruction to a given node. Will do nothing if the given
     * element already has a {@value #PI_CHANGE_DELETED} processing instruction to prevent duplicate instructions.
     * @param element The element being marked as deleted
     */
    public static void markDeleted(final Element element) {
        addProcessingInstructionIfNotExists(element, PI_CHANGE_DELETED);
    }

    /**
     * Checks if the given node has the {@value #PI_CHANGE_DELETED} processing instruction.
     * @param element the element being checked for the processing instruction
     * @return {@code true} if the given element has at least one {@value #PI_CHANGE_DELETED} processing instruction
     */
    public static boolean isDeleted(final Element element) {
        return !DomUtils.findChildProcessingInstructions(element, PI_CHANGE_DELETED).isEmpty();
    }

    /**
     * Adds the {@value #PI_CHANGE_INSERTED} processing instruction to a given node. Will do nothing if the given
     * element already has a {@value #PI_CHANGE_INSERTED} processing instruction to prevent duplicate instructions.
     * @param element The element being marked as inserted
     */
    public static void markInserted(final Element element) {
        addProcessingInstructionIfNotExists(element, PI_CHANGE_INSERTED);
    }

    /**
     * Checks if the given node has the {@value #PI_CHANGE_INSERTED} processing instruction. Please not that an
     * element can be both inserted and deleted, so {@link #isDeleted} might also return {@code true}.
     * @param element the element being checked for the processing instruction
     * @return {@code true} if the given element has at least one {@value #PI_CHANGE_INSERTED} processing instruction
     */
    public static boolean isInserted(final Element element) {
        return !DomUtils.findChildProcessingInstructions(element, PI_CHANGE_INSERTED).isEmpty();
    }

    private static ProcessingInstruction addProcessingInstructionIfNotExists(final Element element, final String pi) {
        if (!DomUtils.findChildProcessingInstructions(element, pi).isEmpty()) {
            // PI already exists, so do not add a new one
            return null;
        }
        return DomUtils.addChildProcessingInstruction(element, pi);
    }

    private static List<ProcessingInstruction> findNamedProcessingInstructions(final Element parent,
                                                                               final String target, final String name) {
        List<ProcessingInstruction> retval = new ArrayList<ProcessingInstruction>(1);
        // first, try to find existing PI with name="xxxx"
        String findName = buildProcessingInstructionData(CHANGE_DATA_NAME, name);
        for (ProcessingInstruction pi : DomUtils.findChildProcessingInstructions(parent, target)) {
            String data = pi.getData();
            if (data.contains(findName)) {
                // existing PI found
                retval.add(pi);
            }
        }
        return retval;
    }

    private static ProcessingInstruction addNamedProcessingInstructionIfNotExists(final Element element,
                                                                                  final String piTarget,
                                                                                  final String name) {
        // abort when existing existing PI with name="xxxx"
        if (!findNamedProcessingInstructions(element, piTarget, name).isEmpty()) {
            return null;
        }
        ProcessingInstruction pi = DomUtils.addChildProcessingInstruction(element, piTarget);
        pi.setData(buildProcessingInstructionData(CHANGE_DATA_NAME, name));
        return pi;
    }

    private static String buildProcessingInstructionData(final String name, final String value) {
        return name + "=\"" + value + "\"";
    }

}

package org.adfemg.datacontrol.xml.provider.filter;

import java.util.List;

import javax.xml.XMLConstants;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Element;

/**
 * Combines the elements returned by the nested DataProviders under a single (new) root element.<br/>
 * The resulting XML can be used directly in a datacontrol, but it is also likely that this UnionFilter
 * is wrapped by a XSLTransformFilter to somehow merge/combine the nested elements.<br/>
 * This UnionFilter requires two parameters in the DataControls.dcx; root-element and root-namespace
 * specifying the element name and namespace.<br/>
 * With the root-element parameter set to {@code foo}, the root-namespace parameter set to
 * {@code bar} and two nested DataFilters returning {@code <baz/>}, {@code <qux/>},
 * this UnionFilter would return:</br>
 * <pre>
 * &lt;ns0:foo xmlns:ns0="bar"&gt;
 * &nbsp;&nbsp;&lt;baz/&gt;
 * &nbsp;&nbsp;&lt;qux/&gt;
 * &lt;/ns0:foo&gt;
 * </pre>
 */
public class UnionFilter extends MultiDataFilter {

    public static final String PARAM_ROOT = "root-element";
    public static final String PARAM_ROOT_NS_URI = "root-namespace";

    public UnionFilter(List<DataProvider> sources) {
        super(sources);
    }

    /**
     * Creates a new root element and appends each e;ement from the nested DataProviders as children of this
     * new root element.<br/>
     * <b>warning:</b> This will invoke org.w3c.dom.Document#adoptNode to move the Elements from the nested
     * DataProviders to the result of this UnionFilter. This removes these elements from the nested DataProviders'
     * reponse documents. If this is a problem, such as with a nested CacheFilter, ensure the nested DataProvider
     * returns a clone of the element and not the source element itself. For a CacheFilter this can easily be done with
     * the {@code clone} parameter.
     */
    @Override
    public Element getRootElement(DataRequest request) {
        final String elemName = getStringParameter(PARAM_ROOT, request, UnionFilter.class, null);
        final String elemNamespace = getStringParameter(PARAM_ROOT_NS_URI, request, UnionFilter.class, XMLConstants.NULL_NS_URI);
        if (elemName == null) {
            throw new IllegalArgumentException("mandatory parameter " + PARAM_ROOT + " unknown");
        }
        final Element retval = XmlFactory.newDocumentElement(elemNamespace, elemName);
        for (DataProvider source : getSources()) {
            final Element child = source.getRootElement(request);
            if (child != null) {
                retval.appendChild(retval.getOwnerDocument().adoptNode(child));
            }
        }
        return retval;
    }

}

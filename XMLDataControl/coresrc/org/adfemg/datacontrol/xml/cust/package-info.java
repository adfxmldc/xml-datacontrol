/**
 * Framework for applying Annotation based customizations to the datacontrol structures and provide runtime
 * implementations of these annotations.
 */
package org.adfemg.datacontrol.xml.cust;


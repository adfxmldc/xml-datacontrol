package org.adfemg.datacontrol.xml.ha;

import java.io.Serializable;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.UserDataHandler;

/**
 * Helper class to store serializable user data in a org.w3c.dom.Node that is retained in serialization of the
 * owning document using SerializableDocument.
 * @param <T> type of org.w3c.dom.Node, most likely Element
 * @see Node#getUserData
 * @see SerializableDocument
 */
public class SerializableUserData<T extends Node> extends AbstractMap<String, Serializable> implements Map<String, Serializable> {

    // key to store single SerializableUserData object in the node's UserData map
    private static final String USER_DATA_KEY = SerializableUserData.class.getName();
    private static final UserDataHandler HANDLER = new Handler();

    private final T node;

    /**
     * Public static factory method to get the single SerializableUserData instance that should exist for
     * a Node. By using this factory method we ensure that only a single SerializableUserData can exist
     * for each node. This is important for serialization so an object graph with multiple references
     * to this single node will all use the same SerializableUserData instance so it will be serialized
     * only once. This prevents each reference to the node to do its own serializion.
     * @param node XML node to be wrapped
     * @return already existing SerializableUserData for the given node or a new instance of
     * SerializableUserData for this node that will also be returned in subsequent calls to
     * forNode for this same node.
     */
    public static <T extends Node> SerializableUserData<T> forNode(T node) {
        return new SerializableUserData<T>(node);
    }

    /**
     * Private constructor so we can ensure only a single SerializableUserData exists for each Node
     * @param node
     * @see #forNode
     */
    private SerializableUserData(T node) {
        this.node = node;
    }

    @Override // from AbstractMap
    public Set<Map.Entry<String, Serializable>> entrySet() {
        return getRealMap().entrySet();
    }

    @Override // from mutable AbstractMap
    public synchronized Serializable put(String key, Serializable value) {
        return getRealMap().put(key, value);
    }

    private Map<String, Serializable> getRealMap() {
        @SuppressWarnings("unchecked")
        Map<String, Serializable> retval = (Map<String, Serializable>) node.getUserData(USER_DATA_KEY);
        if (retval == null) {
            retval = new ConcurrentHashMap<String, Serializable>();
            node.setUserData(USER_DATA_KEY, retval, HANDLER);
            // register node with SerializableDocument so it knows to include this in serialization
            SerializableDocument.forDocument(node.getOwnerDocument()).add(node);
        }
        return retval;
    }

    /**
     * Handler that gets associated with each node we register a SerializableUserData for so we
     * get notified when the node is cloned, deleted, renamed or moving between documents so we
     * can (de)register the SerializableUserData with the owning document or clone/clear the
     * data in SerializableUserData itself.
     */
    private static class Handler implements UserDataHandler {
        @Override
        @SuppressWarnings("fallthrough")
        public void handle(short operation, String key, Object data, Node src, Node dst) {
            switch (operation) {
            case UserDataHandler.NODE_CLONED:
                // duplicate data
                SerializableUserData.forNode(dst).putAll(SerializableUserData.forNode(src));
                break;
            case UserDataHandler.NODE_DELETED:
                // unregister Node with Registry
                SerializableDocument.forDocument(src.getOwnerDocument()).remove(src);
                break;
            case UserDataHandler.NODE_ADOPTED:
                // clean data from target document in case the elemnt is moving, not clone+remove
                SerializableUserData.forNode(src).clear();
            case UserDataHandler.NODE_IMPORTED: // do not transfer data to other document
            case UserDataHandler.NODE_RENAMED:
            default:
            }
        }
    }

}

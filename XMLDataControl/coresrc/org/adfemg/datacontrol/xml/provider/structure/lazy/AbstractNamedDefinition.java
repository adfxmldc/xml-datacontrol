package org.adfemg.datacontrol.xml.provider.structure.lazy;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

import oracle.binding.meta.DataControlDefinition;
import oracle.binding.meta.Definition;
import oracle.binding.meta.NamedDefinition;
import oracle.binding.meta.StructureDefinition;
import oracle.binding.meta.VariableDefinition;

import org.adfemg.datacontrol.xml.provider.structure.MovableNamedDefinition;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public abstract class AbstractNamedDefinition implements MovableNamedDefinition {

    private final TypeMapper typeMapper; // constructor argument

    // normal member variables
    private Hashtable properties;
    private Definition parent;

    public AbstractNamedDefinition(final NamedDefinition parent, final TypeMapper typeMapper) {
        this.parent = parent;
        this.typeMapper = typeMapper;
    }

    @Override
    public Definition getDefinitionParent() {
        return parent;
    }

    @Override
    public Object getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }

    @Override
    public Hashtable getProperties() {
        if (properties == null) {
            properties = new Hashtable();
            properties.putAll(initProperties());
        }
        return properties;
    }

    protected Map<String, ?> initProperties() {
        return Collections.emptyMap();
    }

    public DataControlDefinition getDataControlDefinition() {
        // not required by interface or superclass, but is required by most implementers of this interface
        Definition parent = getDefinitionParent();
        if (parent instanceof StructureDefinition) {
            return ((StructureDefinition) parent).getDataControlDefinition();
        } else if (parent instanceof VariableDefinition) {
            return ((VariableDefinition) parent).getDataControlDefinition();
        } else if (parent instanceof DataControlDefinition) {
            return (DataControlDefinition) parent;
        } else if (parent == null) {
            return null;
        } else {
            throw new IllegalStateException("unsupported definition parent type " + parent.getClass());
        }
    }

    abstract public String getName();

    abstract public String getFullName();

    protected TypeMapper getTypeMapper() {
        return typeMapper;
    }

    /**
     *
     * @param parent StructureDefinition or DataControlDefinition
     */
    @Override
    public void setParent(Definition parent) {
        this.parent = parent;
    }

    abstract public int getDefinitionType();

}

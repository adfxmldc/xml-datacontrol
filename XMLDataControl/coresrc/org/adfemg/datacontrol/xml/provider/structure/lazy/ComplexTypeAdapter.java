package org.adfemg.datacontrol.xml.provider.structure.lazy;

import javax.xml.namespace.QName;

import oracle.binding.meta.ArrayListDefinitionContainer;
import oracle.binding.meta.DefinitionContainer;
import oracle.binding.meta.EmptyDefinitionContainer;
import oracle.binding.meta.NamedDefinition;

import oracle.xml.parser.schema.XSDAttribute;
import oracle.xml.parser.schema.XSDComplexType;
import oracle.xml.parser.schema.XSDConstantValues;
import oracle.xml.parser.schema.XSDElement;
import oracle.xml.parser.schema.XSDNode;
import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.provider.structure.MovableStructureDefinition;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class ComplexTypeAdapter extends AbstractStructureDefinition implements MovableStructureDefinition {

    private final XSDComplexType xsdComplexType; // constructor argument

    private ArrayListDefinitionContainer attributes;
    private ArrayListDefinitionContainer accessors;

    private static final String NS_XMLSCHEMA = "http://www.w3.org/2001/XMLSchema";
    private static final QName QNAME_ANYTYPE = new QName(NS_XMLSCHEMA, XSDConstantValues._anyType);
    private static final QName QNAME_ANYSIMPLETYPE = new QName(NS_XMLSCHEMA, XSDConstantValues._anySimpleType);

    public ComplexTypeAdapter(NamedDefinition parent, String fullName, TypeMapper typeMapper,
                              XSDComplexType complexType) {
        super(parent, fullName, typeMapper);
        this.xsdComplexType = complexType;
    }

    @Override
    public DefinitionContainer getAttributeDefinitions() {
        ensureInit();
        return attributes;
    }

    @Override
    public DefinitionContainer getAccessorDefinitions() {
        ensureInit();
        return accessors;
    }

    @Override
    public DefinitionContainer getOperationDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getConstructorOperationDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getCriteriaDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    private void ensureInit() {
        if (attributes != null) {
            return;
        }
        attributes = new ArrayListDefinitionContainer();
        accessors = new ArrayListDefinitionContainer();

        if (xsdComplexType.isSimpleContent()) {
            // "fake" attribute _value for the SimpleContent for this complexType
            // TODO: implement SimpleContent support (see SchemaStructureProvider#resolveComplexType)
            throw new UnsupportedOperationException("SimpleContent not supported yet");
            // attributes.add(new ComplexTypeSimpleContentAdapter());
        }
        for (XSDAttribute attribute : xsdComplexType.getAttributeDeclarations()) {
            init(attribute);
        }
        // TODO: test how this works with (nested) groups, especially when they are repeating
        final XSDNode[] elements = xsdComplexType.getElementSet();
        if (elements != null) {
            for (XSDNode node : elements) {
                if (node instanceof XSDElement) {
                    init((XSDElement) node);
                } else {
                    throw new UnsupportedOperationException("ensureInit cannot handle " + node.getClass().getName() +
                                                            " " + node.getQName() + " for structure " + getFullName());
                }
            }
        }
    }

    private void init(XSDAttribute xsdAttribute) {
        XSDSimpleType type = (XSDSimpleType) xsdAttribute.getType();
        if (QNAME_ANYSIMPLETYPE.equals(type.getQName()) &&
            (type.getDomNode().getAttribute(XSDConstantValues._type) == null ||
             type.getDomNode().getAttribute(XSDConstantValues._type).isEmpty())) {
            attributes.add(new TypelessAttributeAdapter(this, xsdAttribute.getName(), getTypeMapper()));
        } else {
            attributes.add(new AttributeAdapter(this, xsdAttribute.getName(), getTypeMapper(), xsdAttribute));
        }
    }

    private void init(XSDElement xsdElement) {
        XSDNode type = xsdElement.getType();

        // see if we need to create an attribute (simple type)
        AbstractAttributeDefinition attrDef = null;
        if (type instanceof XSDComplexType && QNAME_ANYTYPE.equals(type.getQName())) {
            final String typeAttr = type.getDomNode().getAttribute(XSDConstantValues._type);
            if (typeAttr == null || typeAttr.isEmpty()) {
                // implicit anyType since no type was set in XSD
                attrDef = new TypelessElementAdapter(this, xsdElement.getName(), getTypeMapper());
            } else {
                // explicit anyType can be handled as any normal simpleType element
                attrDef = new AnyTypeElementAdapter(this, xsdElement.getName(), getTypeMapper());
            }
        } else if (type instanceof XSDSimpleType) {
            // simpleType elements (including anySimpleType) can be handld as any normal element
            attrDef = new SimpleElementAdapter(this, xsdElement.getName(), getTypeMapper(), xsdElement);
        }

        if (attrDef != null) {
            if (xsdElement.getMaxOccurs() > 1) {
                // repeating simple attribute needs wrapper for extra (repeating) StructureDefinition
                // FIXME: RepeatingSimpleElementAdapter should also become parent of attrDef (including in its name)
                accessors.add(new RepeatingScalarAccessor(this, getTypeMapper(), attrDef));
            } else {
                attributes.add(attrDef);
            }
        } else if (type instanceof XSDComplexType) {
            // no attribute definition, so create accessor to complexType
            accessors.add(new ComplexElementAdapter(this, xsdElement.getName(), getTypeMapper(), xsdElement));
        } else {
            throw new UnsupportedOperationException("XSDElement with type " + type.getClass().getName() + " " +
                                                    type.getName());
        }
    }

}

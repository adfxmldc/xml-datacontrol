package org.adfemg.datacontrol.xml.annotation;


/**
 * Helper class for the Annotation Framework.
 */
public class AnnotationHelper {
    /**
     * Default value for the attr string.
     */
    public static final String DFLT_ATTR_STRING = "___DEFAULT___";

    private AnnotationHelper() {
        super();
    }

    /**
     * Check to find out if the attrs are the default value.
     * @param attrs the attributes string array to check.
     * @return {@code true} is the input matches the default,
     *         {@code false} if the input doesn't match the default.
     */
    public static boolean isDefault(String... attrs) {
        // DTAnnotation returns null instead of default value
        return attrs.length == 1 && (attrs[0] == null || DFLT_ATTR_STRING.equals(attrs[0]));
    }

}

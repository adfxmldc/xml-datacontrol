/**
 * Runtime implementation of the Java Reflection API from the org.adfemg.datacontrol.xml.java package that relies on
 * JDK's Reflection API to do the real work.
 */
package org.adfemg.datacontrol.xml.java.rt;


package org.adfemg.datacontrol.xml.ha;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.XPointer;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Since org.w3c.dom.Node doesn't have to support Serialization, this class can be used to wrap
 * a Node to give it Serialization capabilities including the document this Node belongs to so the
 * entire document tree is restored after deserialization.
 * <p>
 * An object graph with multiple instances of SerializableNode that wrap the same Node instance will
 * not lead to multiple clones of the Node after deserialization. Each cloned SerializableNode will
 * point to a single Node even after deserialization of the graph.
 * @see SerializableUserData
 * @see SerializableDocument
 */
public class SerializableNode<T extends Node> implements Serializable {

    @SuppressWarnings("compatibility:-6265816996001340000")
    private static final long serialVersionUID = 2L;

    private transient T node; // custom serialization
    private SerializedState serializedState; // state for lazy deserialization

    /**
     * Create a SerializableNode
     * @param node org.w3c.dom.Node to wrap
     */
    public SerializableNode(T node) {
        this.node = node;
    }

    /**
     * Get the org.w3c.dom.Node contained in this SerializableNode
     * @return org.w3c.dom.Node
     */
    public T getNode() {
        ensureDeserialization();
        return node;
    }

    // custom serialization
    private void writeObject(ObjectOutputStream out) throws IOException {
        if (serializedState != null) {
            // we still have state from previous deserialization (ensureDeserialization wasn't invoked)
            out.writeObject(serializedState);
            return;
        }
        if (node == null) {
            out.writeObject(null);
        } else {
            XPointer xpointer = XPointer.forElement((Element) node);
            SerializableDocument document = SerializableDocument.forDocument(node.getOwnerDocument());
            out.writeObject(new SerializedState(xpointer, document));
        }
    }

    // custom deserialization to delay actual deserialization until the time it is actually needed and not as soon
    // as the object is replicated to other nodes in the cluster. Waiting for deserialization until the actual failover
    // has occured is much more efficient
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        serializedState = (SerializableNode.SerializedState) in.readObject();
    }

    // process any state that was deserialized. Should be invoked by each method that requires object state to ensure
    // full dserialization has occured as readObject delays this
    @SuppressWarnings("unchecked")
    private void ensureDeserialization() {
        if (serializedState == null) {
            return;
        }
        node = (T) serializedState.pointer.resolve(serializedState.document.getDocument());
        serializedState = null;
    }

    @Override
    public String toString() {
        StringBuilder retval = new StringBuilder(this.getClass().getSimpleName()).append("[");
        if (serializedState != null) {
            retval.append("serialized=").append(serializedState.pointer.getXpath());
        } else {
            retval.append(DomUtils.getQName(node));
        }
        retval.append("]");
        return retval.toString();
    }

    private static class SerializedState implements Serializable {
        @SuppressWarnings("compatibility:-4189974989230932608")
        private static final long serialVersionUID = 2L;
        private XPointer pointer;
        private SerializableDocument document;

        private SerializedState(final XPointer pointer, final SerializableDocument document) {
            super();
            this.pointer = pointer;
            this.document = document;
        }
    }

}

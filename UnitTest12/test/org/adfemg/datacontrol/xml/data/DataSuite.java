package org.adfemg.datacontrol.xml.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
                    XMLDCCollectionTest.class, XMLDCCollectionChangeTrackingTest.class,
                    XMLDCElementChangeTrackingTest.class, XMLDCElementTest.class
    })
public class DataSuite {
}

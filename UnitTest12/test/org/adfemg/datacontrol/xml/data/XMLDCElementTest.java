package org.adfemg.datacontrol.xml.data;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class XMLDCElementTest {

    private XMLDCElement xmldcDepartmentList;
    private XMLDCCollection departments;
    private XMLDCElement departmentSales;

    @Before
    public void setUp() throws Exception {
        // create new (empty) DepartmentList Root Element with changtracking off
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(null, false);

        // create new department XMLDCElement with name 'sales' and attribute location 'Utrecht'
        departments = (XMLDCCollection) xmldcDepartmentList.get("Department");
        departmentSales = departments.createElement(-1, xmldcDepartmentList);
        departmentSales.put("name", "sales");
        departmentSales.put("location", "Utrecht");
    }

    @Test
    public void testCreateElement() {
        // assert that the department Element is created with name 'sales' and attribute location 'Utrecht'
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString =
            "<Department location=\"Utrecht\" xmlns=\"http://xmlns.example.com\"><name>sales</name></Department>";
        assertEquals("created department is not correct, name must be 'sales' and attribute location must be 'Utrecht",
                     expectedString, xmlString);
        assertEquals("department name must be 'sales'", "sales", departmentSales.get("name"));
        assertEquals("department attribute location must be 'Utrecht'", "Utrecht", departmentSales.get("location"));
    }

    @Test
    public void testChangeElement() {
        // change the Element name to 'marketing'
        XMLDCElement departmentMarketing = departmentSales;
        departmentMarketing.put("name", "marketing");
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString =
            "<Department location=\"Utrecht\" xmlns=\"http://xmlns.example.com\"><name>marketing</name></Department>";
        assertEquals("department not correct, name must be 'marketing'", expectedString, xmlString);
        assertEquals("department name must be 'marketing'", "marketing", departmentMarketing.get("name"));
        assertEquals("department attribute location must be 'Utrecht'", "Utrecht", departmentMarketing.get("location"));
    }

    @Test
    public void testClearElement() {
        // insert "" for the Element 'name' and assert that the Element is removed
        departmentSales.put("name", "");
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString = "<Department location=\"Utrecht\" xmlns=\"http://xmlns.example.com\"/>";
        assertEquals("department not correct, name must be removed completely", expectedString, xmlString);
        assertNull("name must be removed from the department Element", departmentSales.get("name"));
    }

    @Test
    public void testRemoveElement() {
        // insert null for the Element name and assert that the Element is removed
        departmentSales.put("name", null);
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString = "<Department location=\"Utrecht\" xmlns=\"http://xmlns.example.com\"/>";
        assertEquals("department not correct, name must be removed completely", expectedString, xmlString);
        assertNull("name must be removed from the department Element", departmentSales.get("name"));
    }

    @Test
    public void testChangeAttribute() {
        // change the attribute location to 'Amsterdam'
        departmentSales.put("location", "Amsterdam");
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString =
            "<Department location=\"Amsterdam\" xmlns=\"http://xmlns.example.com\"><name>sales</name></Department>";
        assertEquals("department not correct, name must be 'sales' and attribute location must be 'Amsterdam'",
                     expectedString, xmlString);
        assertEquals("department attribute 'location' must be 'Amsterdam'", "Amsterdam",
                     departmentSales.get("location"));
    }

    @Test
    public void testClearAttribute() {
        // insert "" for the attribute location
        departmentSales.put("location", "");
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString = "<Department xmlns=\"http://xmlns.example.com\"><name>sales</name></Department>";
        assertEquals("department not correct, name must be sales and attribute location must be removed completely",
                     expectedString, xmlString);
        assertNull("attribute location must be removed from the department Element", departmentSales.get("location"));
    }

    @Test
    public void testRemoveAttribute() {
        // remove the attribute completely
        departmentSales.put("location", null);
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString = "<Department xmlns=\"http://xmlns.example.com\"><name>sales</name></Department>";
        assertEquals("department not correct, name must be sales and attribute location must be removed completely",
                     expectedString, xmlString);
        assertNull("attribute location must be removed from the department Element", departmentSales.get("location"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalKey() {
        // add an illegal (non-existing) paramater as key which doesn't comply with the XSD
        XMLDCElement departmentIllegal = departments.createElement(0, xmldcDepartmentList);
        departmentIllegal.put("nameIllegal", "illegal");
    }
}

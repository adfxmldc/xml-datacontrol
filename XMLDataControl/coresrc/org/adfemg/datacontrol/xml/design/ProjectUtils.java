package org.adfemg.datacontrol.xml.design;

import java.net.URL;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.model.adapter.DTContext;
import oracle.adf.model.adapter.MetaDef;
import oracle.adf.share.common.rc.util.MetadataRegistry;
import oracle.adf.share.logging.ADFLogger;

import oracle.adfdtinternal.model.ide.managers.ConfigurationManager;
import oracle.adfdtinternal.model.ide.navigator.nodes.BaseXmlNode;
import oracle.adfdtinternal.model.ide.settings.ADFMSettings;

import oracle.ide.Ide;
import oracle.ide.model.Project;
import oracle.ide.net.URLPath;

import oracle.jdeveloper.java.JavaManager;
import oracle.jdeveloper.model.PathsConfiguration;
import oracle.jdeveloper.webapp.utils.ProjectRunClassPathClassLoaderUtils;

import org.adfemg.datacontrol.xml.utils.NamespaceContextImpl;
import org.adfemg.datacontrol.xml.utils.XmlFactory;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Utility methods for working with JDeveloper projects at design time. Most of the methods will rely on the
 * current datacontrol project which doesn't have to be the selected project in the application navigator or the
 * project of the currently open file in JDeveloper. The current datacontrol project is set in a context whenever the
 * datacontrol framework asks for information from the datacontrol at design time, which is typically when expanding
 * the datacontrol in the datacontrol panel in JDeveloper.
 * <p><b>WARNING:</b> As this class relies on JDeveloper design time classes, you should always check
 * DesignTimeUtils.isDesignTime before invoking anything from this class. At runtime (typically in a weblogic server)
 * this class should never be used as the JDeveloper IDE classes won't be available and there is no such thing as
 * the current datacontrol project.
 */
public final class ProjectUtils {

    private static final String DC_PROJECT_V11_KEY = ProjectUtils.class.getName() + "." + MetaDef.CTX_DC_PROJECT;
    private static final ADFLogger logger = ADFLogger.createADFLogger(ProjectUtils.class);
    private static XPath xpath = null;

    /**
     * Private constructor to prevent instances being created.
     */
    private ProjectUtils() {
    }

    /**
     * Finds a resource (such as a XML file) from the source path or runtime class path of the
     * current datacontrol design time project.
     * <p><b>WARNING:</b> This introduces dependencies on JDeveloper design time classes. Use
     * URLUtils.findResource to locate a resource that will work both at design time and run time.
     * @param resource name/location of the resource to find, for example com/example/path/types.xsd
     * @return URL of the resource if one was found, or {@code null} if none was found
     * @see org.adfemg.datacontrol.xml.utils.URLUtils#findResource
     * @see #getCurrentDataControlProject
     */
    public static URL findResource(String resource) {
        PathsConfiguration paths = ProjectUtils.getCurrentDataControlProjectPaths();
        // try from project source path first, ignoring source paths from dependencies and libs
        URLPath projectSourcePath = paths.getProjectSourcePath();
        URL boundURL = projectSourcePath.toBoundURL(resource);
        if (boundURL != null) {
            return boundURL;
        }
        // when no source found, try from (runtime) classpath (including dependencies and libs)
        URLPath runPath = paths.getRunClassPath();
        return runPath.toBoundURL(resource);
    }

    /**
     * Load a class. Since this needs to return an actual java.lang.Class there is no need to inspect the
     * source path of the current datacontrol design time project. This will only look at the runtime class
     * path of the current DC project including all of its associated libraries and dependencies. If you need to
     * load a class that is part of the projet itself this means the user needs to compile/build the project before
     * invoking this. If the class cannot be located and we are running with a GUI this will show an error dialog,
     * otherwise it will simply throw an exception
     * @param clsName fully qualified name of the class to load
     * @return loaded java.lang.Class
     * @throws AdapterException wrapping a ClassNotFoundException if the class cannot be found and we are not running
     * in design time mode with a graphical user interface
     * @see #getCurrentDataControlProject
     * @see DesignTimeUtils#isGraphicalDesignTime
     */
    public static Class<?> loadClass(final String clsName) {
        final ClassLoader parent = Thread.currentThread().getContextClassLoader();
        final ClassLoader classLoader =
            ProjectRunClassPathClassLoaderUtils.getClassLoader(getCurrentDataControlProject(), parent);
        try {
            return classLoader.loadClass(clsName);
        } catch (ClassNotFoundException e) {
            if (DesignTimeUtils.isGraphicalDesignTime()) {
                MessageUtils.showErrorMessage("Unable to load class",
                                              "Unable to load " + clsName + ". Make sure the " +
                                              getCurrentDataControlProject() +
                                              " project has the appropriate library attached or it is compiled so the .class file exists.");
                return null;
            } else {
                throw new AdapterException(e);
            }
        }
    }

    /**
     * Gets the current design time datacontrol project. This doesn't have to be the selected project in the application
     * navigator or the project of the currently open file in JDeveloper. The current datacontrol project is set in a
     * context by oracle.adfdt.model.datacontrols.JUDTAdapterDataControl whenever the datacontrol framework asks for
     * information from the datacontrol at design time, which is typically when expanding the datacontrol in the
     * datacontrol panel in JDeveloper. If for some reason there is no current design time datacontrol project, this
     * will return the currently active project and log a warning. If that's also not available it will throw an
     * exception.
     * @return current design time datacontrol project or <tt>null</tt> if none is known
     * @throws IllegalStateException if no current datacontrol project is set by JUDTAdapterDataControl and the
     * fallback currently active project is also not known
     */
    public static Project getCurrentDataControlProject() {
        Project project =
            (Project) DTContext.getInstance().get(isVersion11() ? DC_PROJECT_V11_KEY : MetaDef.CTX_DC_PROJECT);
        if (project == null) {
            // could happen if DC loading is triggered by Auditing of currently open file
            project = Ide.getActiveProject();
            logger.warning("no current datacontrol project, fallback to active project " + project,
                           new RuntimeException("debugging aid to show stacktrace who asked for DC project, no real exception"));
        }
        if (project == null) {
            throw new IllegalStateException("No current datacontrol project in context");
        }
        return project;
    }

    /**
     * Get information about all the paths (source, runtime, documentation) as well as project dependencies and
     * other information needed to build a full path.
     * @return PathConfiguration for the current design time datacontrol project or {@code null} if none is known
     * @see #getCurrentDataControlProject
     */
    public static PathsConfiguration getCurrentDataControlProjectPaths() {
        final Project project = getCurrentDataControlProject();
        return project == null ? null : PathsConfiguration.getInstance(project);
    }

    /**
     * Determines the JDeveloper design time project that own the DataControls.dcx file the given
     * Node belongs to. This can be called while loading the datacontrol definition (eg in
     * DataControlDefinition.loadFromMetadata) as that is one of the few times we have access to a
     * XML Node we know belongs to the DataControls.dcx file that datacontrol belongs to.
     * The returned token can be kept and later used to set the appropriate design time project in
     * the context using {@link #setDataControlProjectContext}.
     * <p>This workaround is only needed for JDeveloper 11g as this feature is built-in for JDeveloper
     * 12c. It doesn't hurt to invoke this method in 12c as it will not do anything unless it runs on 11g.
     * @param dataControlNode a xml Node from a DataControls.dcx file
     * @return a token pointing to the JDeveloper project the DataControls.dcx file belongs to and is
     * this the project for that datacontrol and can be used later for loading additional resources
     * from that project's classpath.
     * @see #setDataControlProjectContext
     * @see #clearDataControlProjectContext
     * @see #getCurrentDataControlProject
     */
    public static Object findDataControlProject(final Node dataControlNode) {
        if (!isVersion11()) {
            return null;
        }
        Project retval = null;
        // iterate all projects trying to find the DataControls.dcx this node belongs to
        // for inspiration look at oracle.adfdtinternal.model.ide.jdev.JDevDataControlManager
        // which uses oracle.adfdtinternal.model.ide.providers.DCXDataControlProvider and
        // oracle.adfdtinternal.model.ide.providers.DCXJarDataControlProvider all from oracle.adfm.jar
        for (Project project : Ide.getActiveWorkspace().projects()) {
            if (!ADFMSettings.getInstance(project).hasDataControls()) {
                continue;
            }
            // prevent using DataControlConfigurationNode as that does not exist in JDev 12c
            BaseXmlNode dcxNode = ConfigurationManager.findConfigurationNodeInProject(project);
            if (dcxNode == null) {
                continue;
            }
            Document configDoc = dcxNode.getElement().getOwnerDocument();
            if (configDoc.isEqualNode(dataControlNode.getOwnerDocument())) {
                retval = project;
                break;
            }
        }
        if (retval == null) {
            // after looking at all projects as source folders, scan again in project JARs
            retval = findJarDataControlProject(dataControlNode);
        }
        if (retval == null) {
            // failed to find project for DataControl
            // try to find BeanClass for error reporting
            Node parent = dataControlNode == null ? null : dataControlNode.getParentNode();
            Element parentElem = parent instanceof Element ? (Element) parent : null;
            String beanClass = parentElem.getAttribute("BeanClass");
            logger.warning("Unable to determine design time DC project for {0}",
                           beanClass != null ? beanClass : XmlParseUtils.nodeToString(dataControlNode));
        }
        return retval == null ? null : retval.getURL();
    }

    private static Project findJarDataControlProject(final Node dataControlNode) {
        // build xpath to locate datacontrol we're searching for in its DataControls.dcx
        XPathExpression xpath = buildDataControlXPath((Element) dataControlNode.getParentNode());
        // loop all projects and load all DataControls.dcx files in project's JARs to find our DC
        final MetadataRegistry registry = MetadataRegistry.newInstance();
        for (final Project project : Ide.getActiveWorkspace().projects()) {
            final ClassLoader projectClassLoader = getProjectClassLoader(project);
            DcxVisitor scanner = new DcxVisitor(xpath);
            try {
                registry.visitRegistryPaths(projectClassLoader, /*namespace*/null, MetadataRegistry.DCXREGISTRY,
                                            scanner);
            } catch (Exception e) {
                logger.warning("exception scanning project " + project.getURL() + " for datacontrol", e);
            }
            if (scanner.isFound()) {
                return project;
            }
        }
        return null; // not found
    }

    private static XPathExpression buildDataControlXPath(final Element adapterDcElement) {
        if (xpath == null) {
            final Map<String, String> prefix2ns = new HashMap<String, String>();
            prefix2ns.put("dc", "http://xmlns.oracle.com/adfm/datacontrol");
            prefix2ns.put("cfg", "http://xmlns.oracle.com/adfm/configuration");
            xpath = XmlFactory.newXPath(new NamespaceContextImpl(prefix2ns));
        }
        final String id = adapterDcElement.getAttribute("id");
        final String definition = adapterDcElement.getAttribute("Definition");
        try {
            return xpath.compile(String.format("/cfg:DataControlConfigs/dc:AdapterDataControl[@id='%s' and @Definition='%s']",
                                               id, definition));
        } catch (XPathExpressionException e) {
            logger.warning("error creating xpath", e);
            throw new AdapterException(e);
        }
    }

    /**
     * Returns the java classloader for a given project.
     * @param project JDeveloper project to get the classloader for
     * @return classloader
     * @see oracle.jdeveloper.java.JavaModel#getClassLoader
     */
    public static ClassLoader getProjectClassLoader(final Project project) {
        return JavaManager.getInstance(project).getClassLoader();
    }

    /**
     * Sets the current design time datacontrol project in the design time context so subsequent calls to
     * {@link #getCurrentDataControlProject} will know what project to return. This doesn't have to
     * be the active project in the workspace as users can interact with a datacontrol (for example in the
     * datacontrol palette) while having a file open from a different project.
     * <p><b>WARNING:</b> Be sure to always use a try {} finally {} after setting this context and clearing the
     * context with clearDataControlProjectContext in the finally clause to prevent the project from staying in
     * the context when it is no longer the current DC project.
     * <p>This workaround is only needed for JDeveloper 11g as this feature is built-in for JDeveloper
     * 12c. It doesn't hurt to invoke this method in 12c as it will not do anything unless it runs on 11g.
     * @param projectToken token as returned by {@link #findDataControlProject}
     * @see #findDataControlProject
     * @see #clearDataControlProjectContext
     */
    public static void setDataControlProjectContext(final Object projectToken) {
        if (!isVersion11()) {
            return;
        }
        Project project = null;
        for (Project p : Ide.getActiveWorkspace().projects()) {
            if (p.getURL().equals(projectToken)) {
                project = p;
            }
        }
        DTContext.getInstance().put(DC_PROJECT_V11_KEY, project);
    }

    /**
     * Clears the current design time datacontrol project form the design time context that was previously
     * set by {@link #setDataControlProjectContext}.
     * <p><b>WARNING:</b> Be sure to always use invoke this from a finally {} clause after setting this context
     * to prevent the project from staying in the context when it is no longer the current DC project.
     * <p>This workaround is only needed for JDeveloper 11g as this feature is built-in for JDeveloper
     * 12c. It doesn't hurt to invoke this method in 12c as it will not do anything unless it runs on 11g.
     */
    public static void clearDataControlProjectContext() {
        if (!isVersion11()) {
            return;
        }
        DTContext.getInstance().remove(DC_PROJECT_V11_KEY);
    }

    /**
     * Determines if we're running in JDeveloper/ADF version 11 as that is the only version where we would need
     * the workaround to set/clear the current datacontrol project in the context. JDeveloper 12c handles this
     * out-of-the-box.
     * @return {@code true} if JDeveloper's version number starts with "11."
     */
    private static boolean isVersion11() {
        return Float.toString(Ide.getVersion()).startsWith("11.");
    }

    private static final class DcxVisitor implements MetadataRegistry.PathVisitor2 {
        private final DocumentBuilder docBuilder = XmlFactory.newDocumentBuilder();
        private final XPathExpression xpath;
        private URL foundDcx;

        DcxVisitor(final XPathExpression xpath) {
            this.xpath = xpath;
        }

        /**
         * Visit a single DataControls.dcx file and determine if this is the one we were looking for
         * (if xpath given to the constructor can find a node in the file).
         * @param registry MetadataRegistry
         * @param registrydocument DCX document being analyzed but this is typically {@code null} so we
         * parse it ourselves
         * @param dcxUrl full URL of the DataControls.dcx file within the JAR
         * @param dcxPathOnly path of the DataControls.dcx within its JAR file
         * @param loader classloader
         * @return true to continue scanning or false to abort scanning since we found a match
         */
        @Override
        public boolean visit(MetadataRegistry registry, Document registrydocument, URL dcxUrl, String dcxPathOnly,
                             ClassLoader loader) {
            try {
                Document document = docBuilder.parse(dcxUrl.toExternalForm());
                if (xpath.evaluate(document, XPathConstants.NODE) != null) {
                    // found datacontrol in this DataControls.dcx file
                    this.foundDcx = dcxUrl;
                    return false; // false to abort scan
                }
            } catch (Exception e) {
                logger.warning("exception scanning " + dcxUrl + " for datacontrol", e);
            }
            return true; // true to continue scan
        }

        /**
         * Old-style visiting from before MetadataRegistry.PathVisitor got replaced with
         * MetadataRegistry.PathVisitor2. No longer used if a class implements PathVisitor2.
         * @param registry MetadataRegistry
         * @param dcxUrl full URL of the DataControls.dcx file within the JAR
         * @param dcxPathOnly path of the DataControls.dcx within its JAR file
         * @throws UnsupportedOperationException
         * @deprecated since PathVisitor got superseeded by PathVisitor2
         */
        @Deprecated
        @Override
        public void visit(MetadataRegistry registry, URL dcxUrl, String dcxPathOnly) {
            throw new UnsupportedOperationException();
        }

        /**
         * Has the metadata registry scanning found at least one DataControls.dcx file where the given
         * xpath expression returns a node.
         * @return {@code true} if a matching DataControls.dcx has been found
         */
        public boolean isFound() {
            return foundDcx != null;
        }
    }

}

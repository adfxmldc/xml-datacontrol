package org.adfemg.datacontrol.xml.utils;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

public class CompositeMap<K, V> extends AbstractMap<K, V> {

    private Map<K, V>[] maps;

    public CompositeMap(Map<K, V>... maps) {
        this.maps = maps;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>>[] entrySets = new Set[maps.length];
        for (int i = 0; i < maps.length; i++) {
            entrySets[i] = maps[i].entrySet();
        }
        return new CompositeSet(entrySets);
    }
}

package org.adfemg.datacontrol.xml.provider.customization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import oracle.adf.model.adapter.dataformat.MethodDef;

import org.adfemg.datacontrol.xml.cust.Customizer;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JClassFactory;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;

/**
 * CustomizationProvider that gets the classes with @ElementCustomization annotation from a
 * list-parameter in the DataControls.dcx.
 * <p>For example:
 * <pre>
 * &lt;customization-provider class="org.adfemg.datacontrol.xml.provider.customization.CustomizationProviderImpl">
 *   &lt;parameters>
 *     &lt;list-parameter name="classes">
 *       &lt;value>com.example.DepartmentCust&lt;/value>
 *       &lt;value>com.example.EmployeeCust&lt;/value>
 *     &lt;/list-parameter>
 *   &lt;/parameters>
 * &lt;/customization-provider>
 * </pre>
 * @see CustomizationScanner
 */
public class CustomizationProviderImpl extends ProviderImpl implements CustomizationProvider {

    /**
     * Name of the DataControls.dcx list-parameter that contains the list of java class names.
     */
    public static final String PARAM_CLASSES = "classes";

    /**
     * Perform any necessary customization on the return structure of a datacontrol operation and all of its
     * nested structures. The customization class that have the @ElementCustomization annotation should
     * be explicitly listed in a list-parameter <tt>classes</tt> as explained in the class documentation.
     * @param method the datacontrol operation (and all of its children) to be customized
     */
    @Override
    public void customize(final MethodDef method) {
        new Customizer(getClasses()).customize(method);
    }

    /**
     * Load the annotated java classes from the <tt>classes</tt> list-parameter so they can be used for
     * customizing their targets.
     * @return collection of java classes
     */
    protected Collection<JClass> getClasses() {
        List<String> classNames =
            getRawListParameter(PARAM_CLASSES, CustomizationProviderImpl.class, Collections.<String>emptyList());
        final Collection<JClass> retval = new ArrayList<JClass>(classNames.size());
        for (String className : classNames) {
            JClass jc = JClassFactory.forClass(className);
            retval.add(jc);
        }
        return retval;
    }

}

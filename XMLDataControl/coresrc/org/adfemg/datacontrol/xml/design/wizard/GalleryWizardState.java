package org.adfemg.datacontrol.xml.design.wizard;

import oracle.adf.model.adapter.MetaDef;

import oracle.ide.Context;
import oracle.ide.Ide;
import oracle.ide.model.Project;

import oracle.jdeveloper.model.JavaProject;
import oracle.jdeveloper.wizard.common.BaliWizardState;

import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.ELDataProvider;
import org.adfemg.datacontrol.xml.provider.data.ResourceDataProvider;
import org.adfemg.datacontrol.xml.provider.data.WSDataProvider;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GalleryWizardState implements BaliWizardState {

    private final Context context;
    private Project clientProject;

    private String name;
    private String pkgName;
    private String schema;
    private String schemaDT;
    private String schemaRoot;
    private String dataProvider;

    public GalleryWizardState(Context context) {
        this.context = context;
        this.clientProject = context != null ? context.getProject() : Ide.getActiveProject();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchemaDT(String schemaDT) {
        this.schemaDT = schemaDT;
    }

    public String getSchemaDT() {
        return schemaDT;
    }

    public void setSchemaRoot(String schemaRoot) {
        this.schemaRoot = schemaRoot;
    }

    public String getSchemaRoot() {
        return schemaRoot;
    }

    public void setDataProvider(String dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getDataProvider() {
        return dataProvider;
    }

    public Project getProject() {
        return clientProject;
    }

    public String getDefaultPackage() {
        return JavaProject.getInstance(getProject()).getMostRecentPackage();
    }

    @Override
    public void commitWizardState() {
        Document doc = XmlFactory.newDocument();

        // build definition element for in DataControls.dcx
        Element defNode =
            doc.createElementNS(DataControlDefinitionNode.NS_DEFINITION, DataControlDefinitionNode.ELEM_DEFINITION);
        defNode.setAttribute("xmlns", DataControlDefinitionNode.NS_DEFINITION);
        defNode.setAttribute(DataControlDefinitionNode.DEF_ATTR_SCHEMA, getSchema());
        String dtSchema = getSchemaDT();
        if (dtSchema != null && !dtSchema.isEmpty()) {
            defNode.setAttribute(DataControlDefinitionNode.DEF_ATTR_SCHEMA_DT, dtSchema);
        }
        defNode.setAttribute(DataControlDefinitionNode.DEF_ATTR_SCHEMA_ROOT, getSchemaRoot());
        defNode.setAttribute(DataControlDefinitionNode.DEF_ATTR_DC_OPERATION,
                             DataControlDefinitionNode.DFLT_DC_OPERATION);

        Element dataProv =
            doc.createElementNS(DataControlDefinitionNode.NS_DEFINITION, DataControlDefinitionNode.ELEM_DATA_PROVIDER);
        defNode.appendChild(dataProv);
        dataProv.setAttribute(DataControlDefinitionNode.PROV_ATTR_CLASS, getDataProvider());

        DataProviderType dpt = DataProviderType.valueByClassName(getDataProvider());
        if (dpt != DataProviderType.EMPTY_ELEMENT) {
            Element params =
                doc.createElementNS(DataControlDefinitionNode.NS_DEFINITION, DataControlDefinitionNode.ELEM_PARAMETERS);
            dataProv.appendChild(params);
            if (dpt == DataProviderType.WEBSERVICE) {
                params.appendChild(doc.createComment(" use either end point URL or end point connection name "));
                params.appendChild(buildParam(doc, WSDataProvider.PARAM_END_POINT_URL,
                                              "${soa.server}/services/ExampleService"));
                params.appendChild(buildParam(doc, WSDataProvider.PARAM_END_POINT_CONNECTION, "ExampleConnection"));
                params.appendChild(doc.createComment(" specify optional soapAction from soap:operation in wsdl to enable WS-Addressing SOAPAction http request header "));
                params.appendChild(buildParam(doc, WSDataProvider.PARAM_SOAP_ACTION_URI, ""));
                params.appendChild(doc.createComment(" when using default version 1.1 the soapVersion parameter can be removed. It can also be used to specify non-default version 1.2 "));
                params.appendChild(buildParam(doc, WSDataProvider.PARAM_SOAP_VERSION, "1.1"));
                Element xmlReqParam =
                    doc.createElementNS(DataControlDefinitionNode.NS_DEFINITION,
                                        DataControlDefinitionNode.ELEM_XML_PARAM);
                xmlReqParam.setAttribute(DataControlDefinitionNode.PARAM_ATTR_NAME, WSDataProvider.PARAM_REQUEST_ELEM);
                params.appendChild(xmlReqParam);
                xmlReqParam.appendChild(doc.createComment(" add service request XML (SOAPBody child) within CDATA for proper namespace handling "));
                xmlReqParam.appendChild(doc.createCDATASection(" <ExampleRequest xmlns=\"http://xmlns.example.org/\"/> "));
            } else if (dpt == DataProviderType.EXPR_LANGUAGE) {
                params.appendChild(buildParam(doc, ELDataProvider.PARAM_ROOTELEMENT_EL_EXPR, "#{example}"));
            } else if (dpt == DataProviderType.RESOURCE) {
                params.appendChild(buildParam(doc, ResourceDataProvider.PARAM_RESOURCE, "com/example/sample.xml"));
            } else if (dpt == DataProviderType.CUSTOM) {
                dataProv.insertBefore(doc.createComment(" specify your own implementation of " +
                                                        DataProvider.class.getName() + " as class attribute "), params);
                params.appendChild(doc.createComment("you can add fixed parameters, dynamic parameters, list parameters or xml parameters"));
            }
        }

        DataControlDefinition def = new DataControlDefinition();
        // full name is used for Definition and BeanClass properties in AdapterDataControl
        String fullName = getPkgName() + "." + getName();
        def.setFullName(fullName);
        def.setMetadata(defNode);
        MetaDef metaDef = new MetaDef(MetaDef.CTX_JDEVELOPER);
        metaDef.createDataControl(def);
        MessageUtils.showInformationMessage("XML Data Control",
                                            "Please click the Refresh button in the Data Controls panel when finished");
    }

    private Element buildParam(Document doc, String name, String value) {
        Element retval =
            doc.createElementNS(DataControlDefinitionNode.NS_DEFINITION, DataControlDefinitionNode.ELEM_PARAM);
        retval.setAttribute(DataControlDefinitionNode.PARAM_ATTR_NAME, name);
        retval.setAttribute(DataControlDefinitionNode.PARAM_ATTR_VALUE, value);
        return retval;
    }

}

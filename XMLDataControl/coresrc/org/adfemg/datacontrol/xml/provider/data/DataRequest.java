package org.adfemg.datacontrol.xml.provider.data;

import java.util.Map;
import java.util.Set;

import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.provider.DynamicParameter;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public interface DataRequest {

    /**
     * The StructureDefinition the response to this request should comply to.
     */
    StructureDefinition getStructureDefinition();

    /**
     * Returns unmodifiable map of dynamic parameter values as provided by the DataControl (from
     * the binding layer) or by an upstream DataFilter. When chaining DataFilters do not modify the
     * values in this map but create a new map with parameters and clone/duplicate the object you
     * want to change before passing it to the nested DataProvider. The reason behind this is
     * that the DataControl will use the content of this map to determine whether two requests
     * are the same and a cached result can be returned after a failover in a high-availability
     * cluster. If the objects in the map itself would be modified the map would not be equal to the
     * "same" subsequent request.
     * @return unmodifiable map of dynamic parameters
     */
    Map<String, Object> getDynamicParamValues();

    Set<DynamicParameter> getDynamicParams();

    TypeMapper getTypeMapper();

    XMLSchema getSchema() throws XSDException;

    String getRootNodeName();

}

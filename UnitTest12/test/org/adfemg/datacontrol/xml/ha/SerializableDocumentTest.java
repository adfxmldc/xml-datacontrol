package org.adfemg.datacontrol.xml.ha;

import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


public class SerializableDocumentTest {

    @Test
    public void forDocumentShouldCreateWrapper() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        assertNotNull("forDocument(document) should return (new) SerializableDocument if it not already existed",
                      SerializableDocument.forDocument(document));
    }

    @Test
    public void repeatedForDocumentShouldReturnSameWrapper() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        assertTrue("repeated calls to forDocument(document) should return same SerializableDocument",
                   serdoc == SerializableDocument.forDocument(document));
    }

    @Test
    public void canAddNode() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        serdoc.add(document.getDocumentElement());
    }

    @Test()
    public void canAddNodeNotAddedToDocument() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        Element newElement =
            document.createElementNS(document.getDocumentElement().getNamespaceURI(), "unboundedElement");
        assertTrue("new element should have owner document", document.isSameNode(newElement.getOwnerDocument()));
        serdoc.add(newElement);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotAddNodeFromOtherDocument() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        Document document2 = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        // you shouldn't be able to register node from one document with registry from another document
        serdoc.add(document2.getDocumentElement());
    }

    @Test
    public void initialNodeShouldNotHaveState() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        assertFalse("element shoud not have initial state", serdoc.nodeHasState(document.getDocumentElement()));
    }

    @Test
    public void addedNodeShouldNotHaveState() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        Element element = document.getDocumentElement();
        serdoc.add(element);
        assertTrue("element shoud have state after registration", serdoc.nodeHasState(element));
    }

    @Test
    public void removedNodeShouldNoLongerHaveState() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        Element element = document.getDocumentElement();
        serdoc.add(element);
        serdoc.remove(element);
        assertFalse("element shoud not have state after remove()", serdoc.nodeHasState(element));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotRemoveNodeFromOtherDocument() throws SAXException {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        Document document2 = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        // you shouldn't be able to register node from one document with registry from another document
        serdoc.remove(document2.getDocumentElement());
    }

    @Test
    public void keepDocElementStateOnSerialization() throws Exception {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        Element element = document.getDocumentElement();
        // put UserData state on node in source document
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");

        // create and clone SerializableDocument (which should include UserDataMap)
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        SerializableDocument clone = ClassUtils.cloneUsingSerialization(serdoc);

        assertEquals("Element UserData should survive (de)serialization", "value",
                     SerializableUserData.forNode(clone.getDocument().getDocumentElement()).get("key"));
    }

    @Test
    public void keepSubElementStateOnSerialization() throws Exception {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        Element element = document.getDocumentElement();
        Element subElement = document.createElementNS(element.getNamespaceURI(), "subelement");
        element.appendChild(subElement);
        // put UserData state on sub-element in source document
        SerializableUserData dataMap = SerializableUserData.forNode(subElement);
        dataMap.put("key", "value");

        // create and clone SerializableDocument (which should include UserDataMap)
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        SerializableDocument clone = ClassUtils.cloneUsingSerialization(serdoc);

        Element targetSubElement = (Element) clone.getDocument().getDocumentElement().getChildNodes().item(0);
        assertNotNull("finding sub-element in cloned document", targetSubElement);
        assertEquals("cloned sub-element named subelement", "subelement", targetSubElement.getLocalName());
        assertEquals("Element UserData should survive (de)serialization", "value",
                     SerializableUserData.forNode(targetSubElement).get("key"));
    }

    @Test
    public void secondSerializationShouldDetectChanges() throws Exception {
        Document document = XmlParseUtils.parse("<element xmlns='example.com/ns'/>");
        SerializableDocument serdoc = SerializableDocument.forDocument(document);
        // serialize SerializableDocument for first time to make it create internal serializedState
        ClassUtils.cloneUsingSerialization(serdoc);
        // now change internal state of SerializableDocument
        SerializableUserData.forNode(document.getDocumentElement()).put("key", "value");
        // now clone/serialize SerializableDocument again to make sure it does not use initial serializedState
        SerializableDocument clone = ClassUtils.cloneUsingSerialization(serdoc);
        assertTrue("second cloned SerializableDocument should have UserData",
                   clone.nodeHasState(clone.getDocument().getDocumentElement()));
        assertEquals("second cloned SerializableDocument should have same UserData", "value",
                     SerializableUserData.forNode(clone.getDocument().getDocumentElement()).get("key"));
    }

}

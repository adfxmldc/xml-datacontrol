package org.adfemg.datacontrol.xml.utils;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;

/**
 * Utility methods for loading resources from URLs.
 */
public final class URLUtils {

    private static final String SYSPROP_HANDLER_PKGS = "java.protocol.handler.pkgs";
    private static final String MDS_HANDLER_PKG = "oracle.mds.net.protocol";

    private static final ADFLogger logger = ADFLogger.createADFLogger(URLUtils.class);

    /**
     * Private constructor to prevent instances being created.
     */
    private URLUtils() {
    }

    /**
     * Try to find a given resource in a couple of ways and return it as a URL.
     * <p>The first method that succeeds in locating this resource will define the result. Here
     * are the methods in order:
     * <ol>
     *   <li>interpret the given resource as a URL (for example http://example.com/something)</li>
     *   <li>use the given resource as a filename and verify if it points to a readable file</li>
     *   <li>try loading the resource from the classpath</li>
     *   <li>if the resource starts with /, remove this initial slash and retry loading it as a
     *       classpath resource for when users assume we are using a relative classpath loader</li>
     * </ol>
     * @param resource the resource to load, typically a absolute URL or a classpath resource like
     * {@code com/example/myapp/test.xsd}
     * @return the URL of the located resource or {@code null} if all methods fail.
     * @see Utils#getWorkspaceClassLoader
     */
    public static URL findResource(String resource) {
        ensureMdsHandlers(); // for running outside container with simple class with main method
        URL retval = null;
        // first try if the given resource is a full URL
        if (retval == null) {
            retval = findResourceUrl(resource);
        }
        // when not found yet, try if it is a (relative) path to a file on filesystem
        if (retval == null) {
            retval = findResourceFile(resource);
        }
        // when not found yet, try to load it as a classpath resource
        if (retval == null) {
            retval = findResourceClasspath(resource);
            if (retval == null && resource.startsWith("/")) {
                // retry as classpath resource after removing initial /
                retval = findResourceClasspath(resource.substring(1));
                if (retval != null) {
                    logger.warning("classpath resource {0} should not begin with /", resource);
                }
            }

        }
        // return whatever we got, or null if none of the methods worked
        return retval;
    }

    private static URL findResourceUrl(String resource) {
        try {
            return new URL(resource);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private static URL findResourceFile(String resource) {
        File file = new File(resource);
        if (file.canRead()) {
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException e) {
                return null;
            }
        }
        return null;
    }

    private static URL findResourceClasspath(String resource) {
        if (DesignTimeUtils.isDesignTime()) {
            return org.adfemg.datacontrol.xml.design.ProjectUtils.findResource(resource);
        } else {
            // not using design time. Use simple ClassLoader at runtime
            ClassLoader cloader = Thread.currentThread().getContextClassLoader();
            return cloader.getResource(resource); // gives null if resource not found.
        }
    }

    /**
     * Ensures that the oramds URLs can be retrieved by adding the
     * {@code oracle.mds.net.protocol} to the system property
     * {@code java.protocol.handler.pkgs} if this is not done yet.
     */
    public static void ensureMdsHandlers() {
        String handlerPkgs = System.getProperty(SYSPROP_HANDLER_PKGS);
        if (handlerPkgs == null) {
            System.setProperty(SYSPROP_HANDLER_PKGS, MDS_HANDLER_PKG);
        } else if (!handlerPkgs.contains(MDS_HANDLER_PKG)) {
            System.setProperty(SYSPROP_HANDLER_PKGS, handlerPkgs + "|" + MDS_HANDLER_PKG);
        }
    }

}

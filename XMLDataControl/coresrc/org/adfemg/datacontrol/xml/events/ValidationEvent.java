package org.adfemg.datacontrol.xml.events;

import org.adfemg.datacontrol.xml.DataControl;

/**
 * ValidationEvent extending the DataControlEvent.
 *
 * @see DataControlEvent
 */
public class ValidationEvent extends DataControlEvent {
    @SuppressWarnings("compatibility:-7584984889097091532")
    private static final long serialVersionUID = 1L;

    /**
     * Public constructor for the ValidationEvent, calls the super with the
     * input argument DataControl.
     *
     * @param dataControl the DataControl to use in the call to the super.
     */
    public ValidationEvent(final DataControl dataControl) {
        super(dataControl);
    }
}

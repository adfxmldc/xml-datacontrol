package org.adfemg.datacontrol.xml.provider.data;


public class DataProvidingException extends RuntimeException {
    @SuppressWarnings("compatibility:3641025146970240062")
    private static final long serialVersionUID = 1L;

    public DataProvidingException(String msg) {
        super(msg);
    }

    public DataProvidingException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

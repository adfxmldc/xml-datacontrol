package org.adfemg.datacontrol.xml.provider;

import org.adfemg.datacontrol.xml.provider.structure.StructureProviderSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ StructureProviderSuite.class })
public class ProviderSuite {
}

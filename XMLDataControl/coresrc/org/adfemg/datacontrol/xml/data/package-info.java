/**
 * Runtime data structures for XML DataControl, like XMLDCElement and XMLDCCollection.
 */
package org.adfemg.datacontrol.xml.data;


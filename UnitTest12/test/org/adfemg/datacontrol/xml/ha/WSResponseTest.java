package org.adfemg.datacontrol.xml.ha;

import org.adfemg.datacontrol.xml.provider.data.WSDataProvider;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.XmlFactory;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WSResponseTest {
    public WSResponseTest() {
        super();
    }

    @Test
    public void keepWSResponseElementStateOnSerialization() throws Exception {
        // Call the HR WebService
        Element element = getWSDataProvider().getRootElement(null);
        // Create and clone SerializableDocument
        Element cloneElem = ClassUtils.cloneUsingSerialization(element);

        assertEquals("Cloned element not equals.", XmlParseUtils.nodeToString(element),
                     XmlParseUtils.nodeToString(cloneElem));
    }

    @Test
    public void keepWSResponseElementInDocStateOnSerialization() throws Exception {
        // Call the HR WebService and parse to a Document.
        Element element = getWSDataProvider().getRootElement(null);
        // Create and clone SerializableDocument
        Document retDoc = XmlParseUtils.parse(XmlParseUtils.nodeToString(element));
        // Create and clone SerializableDocument
        SerializableDocument serdoc = SerializableDocument.forDocument(retDoc);
        SerializableDocument clone = ClassUtils.cloneUsingSerialization(serdoc);

        assertEquals("Cloned toString Element in Document not equals.",
                     XmlParseUtils.nodeToString(serdoc.getDocument()),
                     XmlParseUtils.nodeToString(clone.getDocument().getDocumentElement()));
    }

    private Element getRequestElement() {
        Document doc = XmlFactory.newDocument();
        Element de = doc.createElementNS("http://adfemg.org/HR", "DepartmentEmployeesRequest");
        Element df = doc.createElementNS("http://adfemg.org/HR", "departmentId");
        df.setTextContent("30");
        de.appendChild(df);
        return de;
    }

    private WSDataProvider getWSDataProvider() {
        WSDataProvider wsdp = new WSDataProvider();
        wsdp.setXmlParameter("requestElement", getRequestElement());
        wsdp.setStringParameter("endPointUrl", "http://xmldc-sample.appspot.com/HumanResourcesService");
        return wsdp;
    }
}

package org.adfemg.datacontrol.demo;

import com.googlecode.objectify.Ref;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.adfemg.datacontrol.demo.persist.Department;
import org.adfemg.datacontrol.demo.persist.Employee;
import org.adfemg.hr.Location;
import org.adfemg.hr.TDepartment;
import org.adfemg.hr.TEmployee;
import org.adfemg.hr.TJob;

public class Mapper {

    public static TDepartment toSOA(Department dept) {
        if (dept == null) {
            return null;
        }
        TDepartment retval = new TDepartment();
        retval.setId(toSOA(dept.getId()));
        retval.setName(dept.getName());
        retval.setManager(toSOA(dept.getManager()));
        retval.setLocation(toSOA(dept.getLocation()));
        List<TEmployee> emps = empRefsToSOA(dept.getEmployees());
        if (!emps.isEmpty()) {
            retval.setEmployees(new TDepartment.Employees());
            retval.getEmployees().getEmployee().addAll(empRefsToSOA(dept.getEmployees()));
        }
        return retval;
    }

    public static List<TDepartment> deptsToSOA(List<Department> depts) {
        if (depts == null) {
            return Collections.emptyList();
        }
        List<TDepartment> retval = new ArrayList<TDepartment>(depts.size());
        for (Department dept : depts) {
            retval.add(toSOA(dept));
        }
        return retval;
    }

    public static List<TEmployee> empRefsToSOA(List<Ref<Employee>> emps) {
        if (emps == null) {
            return Collections.emptyList();
        }
        List<TEmployee> retval = new ArrayList<TEmployee>(emps.size());
        for (Ref<Employee> emp : emps) {
            retval.add(toSOA(emp.get()));
        }
        return retval;
    }

    public static TEmployee toSOA(Employee employee) {
        if (employee == null) {
            return null;
        }
        TEmployee retval = new TEmployee();
        retval.setId(toSOA(employee.getId()));
        retval.setFirstName(employee.getFirstName());
        retval.setLastName(employee.getLastName());
        retval.setEmail(employee.getEmail());
        retval.setPhoneNumber(employee.getPhoneNumber());
        retval.setHireDate(toSOA(employee.getHireDate()));
        retval.setJob(toSOA(employee.getJob()));
        retval.setSalary(toSOA(employee.getSalary()));
        retval.setCommissionPercentage(toSOA(employee.getCommissionPercentage()));
        return retval;
    }

    public static TJob toSOA(org.adfemg.datacontrol.demo.persist.Job job) {
        if (job == null) {
            return null;
        }
        TJob retval = new TJob();
        retval.setId(job.getId());
        retval.setTitle(job.getTitle());
        retval.setMinSalary(toSOA(job.getMinSalary()));
        retval.setMaxSalary(toSOA(job.getMaxSalary()));
        return retval;
    }

    public static Location toSOA(org.adfemg.datacontrol.demo.persist.Location location) {
        if (location == null) {
            return null;
        }
        Location retval  = new Location();
        retval.setId(toSOA(location.getId()));
        retval.setStreetAddress(location.getStreetAddress());
        retval.setPostalCode(location.getPostalCode());
        retval.setCity(location.getCity());
        retval.setStateprovince(location.getStateprovince());
        retval.setCountry(location.getCountry());
        return retval;
    }

    public static BigDecimal toSOA(Double d) {
        return d == null ? null : BigDecimal.valueOf(d);
    }

    public static XMLGregorianCalendar toSOA(Date date) {
        if (date == null) {
            return null;
        }
        try {
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static BigInteger toSOA(Long lng) {
        return lng == null ? null : BigInteger.valueOf(lng);
    }

}

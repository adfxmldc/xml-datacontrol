package org.adfemg.datacontrol.demo.persist;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Load;

import java.util.ArrayList;
import java.util.List;


@Entity
@Cache
public class Department {

    @Id
    private Long id;
    private String name;
    @Load private Ref<Employee> manager;
    @Load private Ref<Location> location;
    @Load private List<Ref<Employee>> employees = new ArrayList<Ref<Employee>>();

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setManager(Employee manager) {
        this.manager = manager == null ? null : Ref.create(manager);
    }

    public Employee getManager() {
        return manager == null ? null : manager.get();
    }

    public void setLocation(Location location) {
        this.location = location == null ? null : Ref.create(location);
    }

    public Location getLocation() {
        return location == null ? null : location.get();
    }

    public void addEmployee(Employee employee) {
        this.employees.add(Ref.create(employee));
    }

    public List<Ref<Employee>> getEmployees() {
        return employees;
    }

}

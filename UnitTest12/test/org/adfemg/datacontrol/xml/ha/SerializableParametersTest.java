package org.adfemg.datacontrol.xml.ha;

import java.util.Collections;

import org.adfemg.datacontrol.xml.utils.ClassUtils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SerializableParametersTest {

    @Test
    public void singleStringValueShouldSurviveSerialization() throws Exception {
        SerializableParameters sp = new SerializableParameters(Collections.singletonMap("key", "value"));
        SerializableParameters clone = ClassUtils.cloneUsingSerialization(sp);
        assertEquals("cloned map should have one element", "value", clone.get("key"));
    }

}

/**
 * Design-time implementation of the Java Reflection API from the org.adfemg.datacontrol.xml.java package that relies on
 * JDeveloper's JavaManager API to do the real work.
 */
package org.adfemg.datacontrol.xml.java.dt;


package org.adfemg.datacontrol.xml.provider.typemap;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.net.URISyntaxException;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.XMLDCConstants;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;


/**
 * Default {@link TypeMapper} to translate XML Schema datatypes to Java datatypes.
 *
 * For possible default XML Schema datatypes see
 * <a href="http://www.w3.org/TR/2004/PER-xmlschema-2-20040318/#built-in-datatypes">de documentatie bij W3C</a>
 *
 * @see TypeMapper
 * @see ProviderImpl
 */
public class TypeMapperImpl extends ProviderImpl implements TypeMapper {
    private static final ADFLogger logger = ADFLogger.createADFLogger(TypeMapperImpl.class);
    private static Map<QName, String> typeMap = new HashMap<QName, String>(); // XSD-type -> Java
    private static Map<QName, QName> mappableCache = new HashMap<QName, QName>();
    private static DatatypeFactory datatypeFactory;

    // Some text, followed by "+", 2 digits, colon, and 2 more digits.
    // First capturing group is timezone fragment.
    private static final Pattern TIMEZONE_PATTERN = Pattern.compile("^(.*)(\\+\\d{2}:\\d{2})$");

    /**
     * Default no-argument constructor.
     */
    public TypeMapperImpl() {
        super();
    }

    /**
     * Get the javaType for a QName (XSD-type).
     *
     * @param typeName The XSD-type as QName.
     * @return the javaType as a String.
     */
    protected String javaType(final QName typeName) {
        return typeMap.get(typeName);
    }

    /**
     * get the nullJavaType.
     * @return always returns "java.lang.String".
     */
    protected String nullJavaType() {
        return "java.lang.String";
    }

    /**
     * Get the Mappable type (QName) based on an XSDSimpleType.
     * We create a local cache for this.
     *
     * If we can't find the QName for the xsdType, we try it for the base of the xsdType.
     *
     * @param xsdType A XSDSimpleType.
     * @return the QName of the xsdType, can be {@code null}.
     */
    @Override
    public final QName getMappableType(final XSDSimpleType xsdType) {
        if (xsdType == null) {
            return null;
        }
        QName qname = xsdType.getQName();
        if (qname == null) { // Anonymous simpleType, get QName on base of xsdType.
            // We can not cache the result, because this works on QName, which is null.
            return getMappableType(xsdType.getBase());
        }
        //Try to get the Result from cache.
        QName retval = mappableCache.get(qname);
        if (retval == null) { // This is the first time we ask for this type.
            // If there is a specific mapping to a java class, the xsdType is mappable,
            // else we try to base type(s).
            retval = javaType(qname) != null ? qname : getMappableType(xsdType.getBase());
            // cache the result for future references.
            mappableCache.put(qname, retval);
        }
        return retval;
    }

    /**
     * Gets the JavaType based on the xsdType.
     *
     * We first look for the QName of the xsdType.
     * Then we look for the Java type for this QName.
     *
     * @param xsdType The xsdType.
     * @return The JavaType as String, can be {@code null}.
     */
    @Override
    public final String getJavaType(final XSDSimpleType xsdType) {
        if (xsdType == null) {
            return nullJavaType();
        }
        QName qname = xsdType.getQName();
        if (qname == null) { // Anonymous simpleType, get QName on base of xsdType.
            // We can not cache the result, because this works on QName, which is null.
            return getJavaType(xsdType.getBase());
        }
        // Call to see if xsdType is mappable.
        QName mappableType = getMappableType(xsdType);
        return javaType(mappableType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object toJava(final String xmlContent, final QName xmlType, final String javaType) {
        try {
            Object retval;
            if (XMLDCConstants.CLSNAME_STRING.equals(javaType)) { // Empty String has to stay an empty string and not become null.
                retval = xmlContent;
            } else if (xmlContent == null || xmlContent.isEmpty()) { // For all other XML types, the empty string becomes null.
                retval = null;
            } else if (XMLDCConstants.CLSNAME_SQLDATE.equals(javaType) ||
                       XMLDCConstants.CLSNAME_UTILDATE.equals(javaType)) {
                if (XMLDCConstants.QNAME_SCHEMA_DATE.equals(xmlType)) { // Extra check on the xsd:date, we don't expect timezone information.
                    if (xmlContent.contains("T")) {
                        throw new IllegalArgumentException("The content of a date (without time) type contains a time: " +
                                                           xmlContent);
                    }
                    // We do not support timezones.
                    Matcher timezone = TIMEZONE_PATTERN.matcher(xmlContent);
                    if (timezone.matches()) {
                        throw new IllegalArgumentException("The content of a date (without time) contains a time: " +
                                                           xmlContent);
                    }
                }
                retval =
                    new java.sql.Date(datatypeFactory.newXMLGregorianCalendar(xmlContent).toGregorianCalendar().getTime().getTime());
            } else if (XMLDCConstants.CLSNAME_XMLDURATION.equals(javaType)) {
                retval = datatypeFactory.newDuration(xmlContent);
            } else if (XMLDCConstants.CLSNAME_BIGDECIMAL.equals(javaType)) {
                retval = new BigDecimal(xmlContent);
            } else if (XMLDCConstants.CLSNAME_BIGINTEGER.equals(javaType)) {
                retval = new BigInteger(xmlContent);
            } else if (XMLDCConstants.CLSNAME_INTEGER.equals(javaType)) {
                retval = Integer.valueOf(xmlContent);
            } else if (XMLDCConstants.CLSNAME_LONG.equals(javaType)) {
                retval = Long.valueOf(xmlContent);
            } else if (XMLDCConstants.CLSNAME_SHORT.equals(javaType)) {
                retval = Short.valueOf(xmlContent);
            } else if (XMLDCConstants.CLSNAME_BYTE.equals(javaType)) {
                retval = Byte.valueOf(xmlContent);
            } else if (XMLDCConstants.CLSNAME_BOOLEAN.equals(javaType)) {
                // boolean can have the following legal literals: {true, false, 1, 0}
                retval = Boolean.valueOf(xmlContent) || "1".equals(xmlContent);
            } else if (XMLDCConstants.CLSNAME_URI.equals(javaType)) {
                try {
                    retval = new java.net.URI(xmlContent);
                } catch (URISyntaxException e) {
                    throw new IllegalArgumentException(e);
                }
            } else if (XMLDCConstants.CLSNAME_BYTEARRAY.equals(javaType)) {
                if (XMLDCConstants.QNAME_SCHEMA_BASE64BIN.equals(xmlType)) {
                    retval = DatatypeConverter.parseBase64Binary(xmlContent);
                } else if (XMLDCConstants.QNAME_SCHEMA_HEXBIN.equals(xmlType)) {
                    retval = DatatypeConverter.parseHexBinary(xmlContent);
                } else {
                    logger.severe("TypeMapper.toJava can not map from xsdType {0} to Java type {1}", new Object[] {
                                  xmlType, javaType
                    });
                    throw new UnsupportedOperationException("TypeMapper.toJava can not map from xsdType " + xmlType +
                                                            " to Java type " + javaType);
                }
            } else {
                logger.severe("TypeMapper.toJava can not map to Java type {0}", javaType);
                throw new UnsupportedOperationException("TypeMapper.toJava can not map to Java type " + javaType);
            }
            if (logger.isFinest()) {
                logger.finest("Mapped \"{0}\" from {1} to {2} as {3}", new Object[] {
                              xmlContent, xmlType, javaType, retval });
            }
            return retval;
        } catch (RuntimeException e) {
            logger.warning("Convert \"" + xmlContent + "\" to " + javaType + " failed ", e);
            throw e;
        }
    }

    /**
     * Utility method to create a Calendar based on a java.util.Date.
     *
     * @param date Date as java.util.Date.
     * @return Calendar with the corresponding date.
     */
    protected Calendar toCalendar(final java.util.Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String toXml(final Object javaValue, final QName xmlType) {
        try {
            String retval;
            if (javaValue == null) {
                retval = "";
            } else if ((null == xmlType || XMLDCConstants.QNAME_SCHEMA_STRING.equals(xmlType) ||
                        XMLDCConstants.QNAME_SCHEMA_NOTATION.equals(xmlType) ||
                        XMLDCConstants.QNAME_SCHEMA_GYEARMONTH.equals(xmlType) ||
                        XMLDCConstants.QNAME_SCHEMA_GMONTHDAY.equals(xmlType))) {
                retval = javaValue == null ? "" : javaValue.toString();
            } else if (XMLDCConstants.QNAME_SCHEMA_DATETIME.equals(xmlType) && javaValue instanceof java.util.Date) {
                retval = DatatypeConverter.printDateTime(toCalendar((java.util.Date) javaValue));
            } else if (XMLDCConstants.QNAME_SCHEMA_TIME.equals(xmlType) && javaValue instanceof java.util.Date) {
                retval = DatatypeConverter.printTime(toCalendar((java.util.Date) javaValue));
            } else if (XMLDCConstants.QNAME_SCHEMA_DATE.equals(xmlType) && javaValue instanceof java.util.Date) {
                retval = DatatypeConverter.printDate(toCalendar((java.util.Date) javaValue));
                // Strip timezone behind the date without time:: 31-12-2011+02:00 becomes 31-12-2011.
                Matcher timezone = TIMEZONE_PATTERN.matcher(retval);
                if (timezone.matches()) {
                    retval = timezone.group(1);
                }
            } else if (XMLDCConstants.QNAME_SCHEMA_FLOAT.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printFloat(((Number) javaValue).floatValue());
            } else if (XMLDCConstants.QNAME_SCHEMA_DECIMAL.equals(xmlType) && javaValue instanceof BigDecimal) {
                retval = DatatypeConverter.printDecimal((BigDecimal) javaValue);
            } else if (XMLDCConstants.QNAME_SCHEMA_INTEGER.equals(xmlType) && javaValue instanceof BigInteger) {
                retval = DatatypeConverter.printInteger((BigInteger) javaValue);
            } else if (XMLDCConstants.QNAME_SCHEMA_LONG.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printLong(((Number) javaValue).longValue());
            } else if (XMLDCConstants.QNAME_SCHEMA_INT.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printInt(((Number) javaValue).intValue());
            } else if (XMLDCConstants.QNAME_SCHEMA_SHORT.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printShort(((Number) javaValue).shortValue());
            } else if (XMLDCConstants.QNAME_SCHEMA_BYTE.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printByte(((Number) javaValue).byteValue());
            } else if (XMLDCConstants.QNAME_SCHEMA_UNSINT.equals(xmlType) && javaValue instanceof Number) {
                retval = DatatypeConverter.printUnsignedInt(((Number) javaValue).longValue());
            } else if ((XMLDCConstants.QNAME_SCHEMA_UNSSHORT.equals(xmlType) ||
                        XMLDCConstants.QNAME_SCHEMA_UNSBYTE.equals(xmlType)) && javaValue instanceof Number) {
                retval = DatatypeConverter.printUnsignedShort(((Number) javaValue).intValue());
            } else if ((XMLDCConstants.QNAME_SCHEMA_DOUBLE.equals(xmlType)) && javaValue instanceof Number) {
                retval = DatatypeConverter.printDouble(((Number) javaValue).doubleValue());
            } else if ((XMLDCConstants.QNAME_SCHEMA_ANYURI.equals(xmlType)) && javaValue instanceof java.net.URI) {
                retval = ((java.net.URI) javaValue).toString();
            } else if (XMLDCConstants.QNAME_SCHEMA_BOOLEAN.equals(xmlType) && javaValue instanceof Boolean) {
                retval = DatatypeConverter.printBoolean((Boolean) javaValue);
            } else if (XMLDCConstants.QNAME_SCHEMA_BASE64BIN.equals(xmlType) && javaValue instanceof byte[]) {
                retval = DatatypeConverter.printBase64Binary((byte[]) javaValue);
            } else if (XMLDCConstants.QNAME_SCHEMA_HEXBIN.equals(xmlType) && javaValue instanceof byte[]) {
                retval = DatatypeConverter.printHexBinary((byte[]) javaValue);
            } else {
                logger.severe("TypeMapper.toXml can not map Java type from {0} to XML type {1}", new Object[] {
                              javaValue.getClass(), xmlType
                });
                throw new UnsupportedOperationException("TypeMapper.toXml can not map Java type from " +
                                                        javaValue.getClass() + " to XML type " + xmlType);
            }
            if (logger.isFinest()) {
                logger.finest("Mapped {0} with value {1} to xml type {2} as {3}", new Object[] {
                              (javaValue == null ? null : javaValue.getClass().getName()), javaValue, xmlType, retval
                });
            }
            return retval;
        } catch (RuntimeException e) {
            logger.warning("Converting " + javaValue + " from Java type " +
                           (javaValue == null ? null : javaValue.getClass()) + " to xml type " + xmlType + " failed ",
                           e);
            throw e;
        }
    }

    /**
     * Creates a new instance of the DatatypeFactory.
     * Fills the typeMap Map from QName to JavaType (as String).
     */
    static {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            logger.warning(e);
            throw new AdapterException(e);
        }
        // Inspired by oracle.adf.model.adapter.dataformat.xml.TypeMap
        // see http://www.w3.org/TR/2004/PER-xmlschema-2-20040318/#built-in-datatypes for types.
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_STRING, XMLDCConstants.CLSNAME_STRING);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_BOOLEAN, XMLDCConstants.CLSNAME_BOOLEAN);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_BASE64BIN, XMLDCConstants.CLSNAME_BYTEARRAY);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_HEXBIN, XMLDCConstants.CLSNAME_BYTEARRAY);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_FLOAT, XMLDCConstants.CLSNAME_BIGDECIMAL);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_DECIMAL, XMLDCConstants.CLSNAME_BIGDECIMAL);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_INTEGER, XMLDCConstants.CLSNAME_BIGINTEGER);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_LONG, XMLDCConstants.CLSNAME_LONG);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_INT, XMLDCConstants.CLSNAME_INTEGER);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_SHORT, XMLDCConstants.CLSNAME_SHORT);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_BYTE, XMLDCConstants.CLSNAME_BYTE);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_UNSINT,
                    XMLDCConstants.CLSNAME_LONG); // unsigned, max 2 times Integer.MAX_VALUE
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_UNSSHORT,
                    XMLDCConstants.CLSNAME_INTEGER); // unsigned max 2 times Integer.MAX_VALUE
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_UNSBYTE,
                    XMLDCConstants.CLSNAME_SHORT); // unsigned max 2 times Byte.MAX_VALUE
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_DOUBLE, XMLDCConstants.CLSNAME_BIGDECIMAL);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_ANYURI, XMLDCConstants.CLSNAME_URI);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_QNAME, XMLDCConstants.CLSNAME_XMLQNAME);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_NOTATION, XMLDCConstants.CLSNAME_STRING);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_DURATION, XMLDCConstants.CLSNAME_XMLDURATION); // jodatime?
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_DATETIME,
                    XMLDCConstants.CLSNAME_SQLDATE); // Note, Oracle: Bug 5697720 - changing mapping from util.Date to sql.Date to support token validations.
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_TIME, XMLDCConstants.CLSNAME_SQLDATE);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_DATE, XMLDCConstants.CLSNAME_SQLDATE);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_GYEARMONTH, XMLDCConstants.CLSNAME_STRING);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_GYEAR, XMLDCConstants.CLSNAME_SQLDATE);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_GMONTHDAY, XMLDCConstants.CLSNAME_STRING);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_GDAY, XMLDCConstants.CLSNAME_SQLDATE);
        typeMap.put(XMLDCConstants.QNAME_SCHEMA_GMONTH, XMLDCConstants.CLSNAME_SQLDATE);
    }
}

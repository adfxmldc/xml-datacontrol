package org.adfemg.datacontrol.xml.java;

import java.util.List;


/**
 * Represents a single java method from a java class.
 */
public interface JMethod {

    /**
     * Returns the name of the method represented by this Method object, as a String.
     * @return the simple name of the underlying member, such as {@code getContent} without any modifiers,
     * return types or arguments.
     */
    public String getName();

    /**
     * Returns a JClass object that represents the formal return type of the method represented by this JMethod object.
     * @return the return type for the method this object represents
     */
    public JClass getReturnType();

    /**
     * Returns the JClass objects that represent the formal parameter types, in declaration order, of the method
     * represented by this JMethod object. Returns an empty list if the underlying method takes no parameters.
     * @return the parameter types for the method this object represents
     */
    public List<JClass> getParameterTypes();

    /**
     * Returns this method's annotation for the specified type if such an annotation is present, else null.
     * @param annotationClass the JClass object corresponding to the annotation type
     * @return this method's annotation for the specified annotation type if present on this method, else null
     */
    public JAnnotation getAnnotation(JClass annotationClass);

    /**
     * Returns a string describing this JMethod, including type parameters. The string is formatted as the method access
     * modifiers, if any, followed by an angle-bracketed comma-separated list of the method's type parameters, if any,
     * followed by the method's generic return type, followed by a space, followed by the class declaring the method,
     * followed by a period, followed by the method name, followed by a parenthesized, comma-separated list of the
     * method's generic formal parameter types. If this method was declared to take a variable number of arguments,
     * instead of denoting the last parameter as "Type[]", it is denoted as "Type...". A space is used to separate
     * access modifiers from one another and from the type parameters or return type. If there are no type parameters,
     * the type parameter list is elided; if the type parameter list is present, a space separates the list from the
     * class name. If the method is declared to throw exceptions, the parameter list is followed by a space, followed
     * by the word throws followed by a comma-separated list of the generic thrown exception types. If there are no
     * type parameters, the type parameter list is elided.
     * @return a string describing this Method, include type parameters, for example
     * {@code public boolean equals(Object o)}
     */
    public String toGenericString();

}

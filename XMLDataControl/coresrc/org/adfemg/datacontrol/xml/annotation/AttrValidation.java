package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to create a validation method on an attribute.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void validate<i>AttrName</i>(AttrChangeEvent&lt;<i>AttributeJavaClass</i>&gt; event) throws AttrValException</code>
 * <p>
 * The name of the annotated method determines which attribute has to
 * be changed for the method to be called. For example an annotated
 * method validateName will be called if the {@code name} attribute is
 * changed. If the attribute name contains illegal characters or starts with
 * an uppercase letter you can also use the attr attribute on the annotation
 * to force the name of the attribute, for example<br>
 * {@code @AttrValidation(attr="Some*name") }
 * <p>
 * When validation fails the annotated method should throw a
 * {@link oracle.jbo.AttrValException} or {@link oracle.jbo.AttrSetValException}.
 * Most of their constructors require a resource bundle (class), but their is
 * one simple alternative without using a resource bundle:
 * <pre>
 * throw new AttrValException(AttrValException.TYP_ATTRIBUTE, "ErrorMessage", "ERR-CODE",
 *                            event.getElement().getDefinition().getFullName(),
 *                            event.getAttribute());
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface AttrValidation {
    String attr() default AnnotationHelper.DFLT_ATTR_STRING;
}

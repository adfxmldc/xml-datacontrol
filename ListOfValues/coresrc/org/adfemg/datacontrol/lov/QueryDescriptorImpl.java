package org.adfemg.datacontrol.lov;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.QueryDescriptor;
import oracle.adf.view.rich.model.QueryModel;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;


public class QueryDescriptorImpl extends QueryDescriptor implements QueryModeHandler {
    private static final ADFLogger logger = ADFLogger.createADFLogger(QueryDescriptorImpl.class);

    private final QueryModel queryModel;
    private final ViewObject viewObject;
    private final Map<String, Object> uiHints = new HashMap<String, Object>();
    private final ConjunctionCriterionImpl conjunctionCriterion;
    private AttributeCriterion autoCompleteCriterion;

    public QueryDescriptorImpl(final QueryModel queryModel, final ConjunctionCriterionImpl crits,
                               final ViewObject viewObject) {
        this.queryModel = queryModel;
        this.viewObject = viewObject;
        this.conjunctionCriterion = crits;
        if (crits != null && crits.getCriterionList() != null &&
            crits.getCriterionList().get(0) instanceof AttributeCriterion) {
            autoCompleteCriterion = (AttributeCriterion) crits.getCriterionList().get(0);
        }
        changeMode(QueryDescriptor.QueryMode.BASIC);
        uiHints.put(QueryDescriptor.UIHINT_DEFAULT, true);
        uiHints.put(QueryDescriptor.UIHINT_IMMUTABLE, true);
        uiHints.put(QueryDescriptor.UIHINT_AUTO_EXECUTE, true); // re-exec after reset
        uiHints.put(QueryDescriptor.UIHINT_NAME, "?");
        uiHints.put(QueryDescriptor.UIHINT_SHOW_IN_LIST, true);
    }

    @Override
    public void addCriterion(final String name) {
        List<AttributeDescriptor> attributes = (List<AttributeDescriptor>) queryModel.getAttributes();
        for (AttributeDescriptor attr : attributes) {
            if (name.equals(attr.getName())) {
                final AttributeCriterionImpl crit = new AttributeCriterionImpl(attr);
                crit.setRemovable(true);
                conjunctionCriterion.addCriterion(crit);
            }
        }
    }

    @Override
    public final void changeMode(final QueryDescriptor.QueryMode mode) {
        uiHints.put(QueryDescriptor.UIHINT_MODE, mode);
        if (conjunctionCriterion != null) {
            conjunctionCriterion.changeMode(mode);
        }
    }

    @Override
    public QueryDescriptor.QueryMode getMode() {
        return (QueryDescriptor.QueryMode) uiHints.get(QueryDescriptor.UIHINT_MODE);
    }

    @Override
    public ConjunctionCriterionImpl getConjunctionCriterion() {
        return conjunctionCriterion;
    }

    @Override
    public String getName() {
        // TODO: implement
        throw new UnsupportedOperationException("QueryDescriptorImpl.getName");
    }

    @Override
    public Map<String, Object> getUIHints() {
        return Collections.unmodifiableMap(uiHints);
    }

    @Override
    public void removeCriterion(final Criterion criterion) {
        conjunctionCriterion.removeCriterion(criterion);
    }

    @Override
    public AttributeCriterion getCurrentCriterion() {
        // TODO: implement (Maybe we want criterion to know wether we're in simple or advanced mode.
        throw new UnsupportedOperationException("QueryDescriptorImpl.getCurrentCriterion");
    }

    @Override
    public void setCurrentCriterion(final AttributeCriterion attrCriterion) {
        // TODO: implement
        throw new UnsupportedOperationException("QueryDescriptorImpl.setCurrentCriterion");
    }

    public void performQuery() {
        final ViewCriteria vc = conjunctionCriterion.toViewCriteria(viewObject);
        viewObject.clearCache();
        viewObject.applyViewCriteria(vc);
        viewObject.executeQuery();
    }

    public void reset() {
        if (ModelImpl.isAutoCompleteRequest()) { // Do not reset the search criteria during an auto-complete Request.
            return;
        }
        conjunctionCriterion.reset();
        // At the moment of an auto-query also fire the query. This is the same behavior
        // as the Framework by clicking the explicite reset button. But not while opening
        // the LOV with the search button.
        // In this case we only call the reset to empty the search arguments, but
        // we still show the search results from the previous search.

        if (Boolean.TRUE.equals(getUIHints().get(QueryDescriptor.UIHINT_AUTO_EXECUTE))) {
            performQuery();
        }
    }

    public AttributeCriterion getAutoCompleteCriterion() {
        return autoCompleteCriterion;
    }

    public void setAutoCompleteCriterion(final AttributeCriterion crit) {
        if (!conjunctionCriterion.getCriterionList().contains(crit)) {
            throw new IllegalArgumentException("auto-complete criterion needs to exist in the conjunctionCriterion");
        }
        autoCompleteCriterion = crit;
    }

    public void setAutoCompleteCriterion(final String attrName) {
        for (Criterion crit : conjunctionCriterion.getCriterionList()) {
            if (crit instanceof AttributeCriterion && !((AttributeCriterion) crit).isRemovable() &&
                attrName.equals(((AttributeCriterion) crit).getAttribute().getName())) {
                setAutoCompleteCriterion((AttributeCriterion) crit);
                return;
            }
        }
        throw new IllegalArgumentException("No AttributeCriterion found for attribute " + attrName);
    }

}

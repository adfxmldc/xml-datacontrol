package org.adfemg.datacontrol.xml.provider.filter;

import java.io.IOException;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataProvidingException;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.utils.URLUtils;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Element;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SchemaValidationFilter extends DataFilter {

    public static final String PARAM_SCHEMA = "schema";
    public static final String PARAM_DISABLE = "disable";
    public static final boolean DFLT_DISABLE = false;
    public static final String PARAM_FAIL_ON_WARNING = "fail-on-warning";
    public static final boolean DFLT_FAIL_ON_WARNING = false;

    public SchemaValidationFilter(DataProvider source) {
        super(source);
    }

    /**
     * Gets the xml element from the nested DataProvider and validates it against an XML schema before returning
     * it upstream.
     * @param request DataRequest
     * @return validated xml element
     * @throws ValidationException when xml element from nested DataProvider fails validation
     */
    @Override
    public Element getRootElement(DataRequest request) {
        Element retval = getSourceElement(request); // get xml element from nested dataprovider
        if (isValidationEnabled(request)) {
            // TODO: can we cache some of this for performance (but be aware of threading issues or dynamic parameters
            // that might change and should be part of the caching key)
            // javadoc from javax.xml.validation.Schema: A Schema object is thread safe and applications are encouraged
            // to share it across many parsers in many threads
            Schema schema;
            try {
                schema = XmlFactory.newSchema(getSchema(request));
            } catch (SAXException e) {
                throw new DataProvidingException("error parsing XML schema", e);
            }
            Validator validator = schema.newValidator();
            CollectingErrorHandler collector = new CollectingErrorHandler();
            validator.setErrorHandler(collector);
            try {
                validator.validate(new DOMSource(retval)); // TODO: can also accept additional Result arg. why?
            } catch (IOException e) {
                throw new DataProvidingException("error reading xml source", e);
            } catch (SAXException e) {
                throw new DataProvidingException("error validating xml", e);
            }
            // now check if any warnings or errors have been collected
            String msg = collector.getCombinedMessage(isFailOnWarning(request));
            if (StringUtils.isNotBlank(msg)) {
                throw new ValidationException("xml validation errors:\n" + msg);
            }
        }
        return retval;
    }

    protected boolean isValidationEnabled(DataRequest request) {
        String disabled =
            getStringParameter(PARAM_DISABLE, request, SchemaValidationFilter.class, Boolean.toString(DFLT_DISABLE));
        return !Boolean.valueOf(disabled);
    }

    /**
     * @param request
     * @return {@code true} if the validation should fail when encountering a warning. This is only true
     * if the fail-on-warning parameter has been explicitly set to {@code true} as the default is {@code false}
     * @see Boolean#valueOf(String)
     */
    protected boolean isFailOnWarning(DataRequest request) {
        String failOnWarning =
            getStringParameter(PARAM_FAIL_ON_WARNING, request, SchemaValidationFilter.class,
                               Boolean.toString(DFLT_FAIL_ON_WARNING));
        return Boolean.valueOf(failOnWarning);
    }

    protected Source getSchema(DataRequest request) {
        String schema = getStringParameter(PARAM_SCHEMA, request, SchemaValidationFilter.class, null);
        if (schema == null) {
            throw new IllegalArgumentException("mandatory parameter " + PARAM_SCHEMA + " unknown");
        }
        URL url = URLUtils.findResource(schema);
        if (url == null) {
            throw new IllegalArgumentException("unable to find schema \"" + schema + "\"");
        }
        try {
            return new StreamSource(url.openStream());
        } catch (IOException e) {
            throw new IllegalArgumentException("unable to load " + url, e);
        }
    }

    public static final class CollectingErrorHandler implements ErrorHandler {

        List<SAXParseException> warnings = new ArrayList<SAXParseException>();
        List<SAXParseException> errors = new ArrayList<SAXParseException>();
        List<SAXParseException> fatalErrors = new ArrayList<SAXParseException>();

        @Override
        public void warning(SAXParseException warning) throws SAXException {
            warnings.add(warning);
        }

        @Override
        public void error(SAXParseException error) throws SAXException {
            errors.add(error);
        }

        @Override
        public void fatalError(SAXParseException fatalError) throws SAXException {
            fatalErrors.add(fatalError);
        }

        public List<SAXParseException> getAllIssues(boolean includeWarnings) {
            List<SAXParseException> retval =
                new ArrayList<SAXParseException>(fatalErrors.size() + errors.size() +
                                                 (includeWarnings ? warnings.size() : 0));
            // first fatal-errors (most important)
            retval.addAll(fatalErrors);
            // then normal errors
            retval.addAll(errors);
            // and potentially the warnings
            if (includeWarnings) {
                retval.addAll(warnings);
            }
            return retval;
        }

        public String getCombinedMessage(boolean includeWarnings) {
            List<SAXParseException> exceptions = getAllIssues(includeWarnings);
            if (exceptions.isEmpty()) {
                return null;
            }
            StringBuilder retval = new StringBuilder(exceptions.size() * 100);
            for (SAXParseException spe : exceptions) {
                if (retval.length() > 0) {
                    retval.append("\n");
                }
                retval.append(buildMessage(spe));
            }
            return retval.toString();
        }

        protected String buildMessage(SAXParseException spe) {
            int line = spe.getLineNumber();
            int column = spe.getColumnNumber();
            String msg = spe.getMessage();
            StringBuilder retval = new StringBuilder(msg + 30);
            if (line > -1) {
                retval.append("line " + line);
            }
            if (column > -1) {
                retval.append(retval.length() > 0 ? ", " : "").append("position ").append(column);
            }
            retval.append(retval.length() > 0 ? ": " : "").append(msg);
            return retval.toString();
        }

    }

    public static final class ValidationException extends DataProvidingException {
        @SuppressWarnings("compatibility:-59644502759500875")
        private static final long serialVersionUID = 1L;

        public ValidationException(String msg) {
            super(msg);
        }
    }

}

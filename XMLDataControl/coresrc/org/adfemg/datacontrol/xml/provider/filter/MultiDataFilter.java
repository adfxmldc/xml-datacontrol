package org.adfemg.datacontrol.xml.provider.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;

import org.w3c.dom.Element;

public abstract class MultiDataFilter extends DataFilter {

    final List<DataProvider> sources;

    public MultiDataFilter(List<DataProvider> sources) {
        super(null);
        if (sources == null || sources.isEmpty()) {
            throw new IllegalArgumentException("MultiDataFilter needs at least one nested DataProvider");
        }
        this.sources = new ArrayList<DataProvider>(sources);
    }

    /**
     * Gets the list of nested DataProvider sources that can be used byu a concrete subclass to
     * choose between the sources or somehow combine their results.
     * @return list of nested DataProviders as supplied to the constructor
     */
    protected List<DataProvider> getSources() {
        return Collections.unmodifiableList(sources);
    }

    /**
     * throws UnsupportedOperationException as a MultiDataFilter has multiple sources
     * and it is up to getRootElement to choose between them or combine them.
     * @param request
     * @return nothing
     * @throws UnsupportedOperationException
     */
    @Override
    protected Element getSourceElement(DataRequest request) {
        throw new UnsupportedOperationException("MultiDataFilter should request element from each source");
    }

    @Override
    public void dataControlCreated(DataControl dc) {
        super.dataControlCreated(dc);
        // let nested DataProviders know our datacontrol has been created
        for (DataProvider dp : sources) {
            dp.dataControlCreated(dc);
        }
    }

}

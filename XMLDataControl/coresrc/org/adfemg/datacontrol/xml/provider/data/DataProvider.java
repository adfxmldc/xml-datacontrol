package org.adfemg.datacontrol.xml.provider.data;

import org.adfemg.datacontrol.xml.provider.Provider;

import org.w3c.dom.Element;


/**
 * The Interface for a provider of XML data for a DataControl.
 *
 * @see Provider
 */
public interface DataProvider extends Provider {

    /**
     * Provides the XML Element to the DataControl.
     * @param request all information supplied by the invoking DataControl that might be needed to fetch the
     *        xml element. Cannot be {@code null}
     * @return The rootElement as an org.w3c.dom.Element.
     */
    Element getRootElement(DataRequest request);
}

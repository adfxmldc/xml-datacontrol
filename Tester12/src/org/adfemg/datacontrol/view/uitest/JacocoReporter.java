package org.adfemg.datacontrol.view.uitest;

import java.io.IOException;
import java.io.Reader;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.tools.ExecFileLoader;
import org.jacoco.report.FileMultiReportOutput;
import org.jacoco.report.IMultiReportOutput;
import org.jacoco.report.IReportGroupVisitor;
import org.jacoco.report.IReportVisitor;
import org.jacoco.report.ISourceFileLocator;
import org.jacoco.report.html.HTMLFormatter;

import org.junit.runner.Description;


public class JacocoReporter {

    private final List<Path> classes;
    private final Path outputdir;
    private final List<Path> src;
    private final Charset srcCharset;
    private final int srcTabWidth;

    private Description description;

    public static Builder builder() {
        return new Builder();
    }

    private JacocoReporter(final List<Path> classes, final Path outputdir, final List<Path> src,
                           final Charset srcCharset, final int srcTabWidth) {
        this.classes = Collections.unmodifiableList(new ArrayList<Path>(classes));
        this.outputdir = outputdir;
        this.src = Collections.unmodifiableList(new ArrayList<Path>(src));
        this.srcCharset = srcCharset;
        this.srcTabWidth = srcTabWidth;
    }

    protected void report(final ExecFileLoader dump, final Description test) throws IOException {
        this.description = test;
        System.out.println("CREATING HTML REPORT IN " + getReportDir());
        final IReportVisitor visitor = createVisitor();
        visitor.visitInfo(dump.getSessionInfoStore().getInfos(), dump.getExecutionDataStore().getContents());
        final ISourceFileLocator sourcelocator = createSourceLocator();
        createReport(visitor, sourcelocator, dump.getExecutionDataStore());
        visitor.visitEnd();

    }

    protected IReportVisitor createVisitor() throws IOException {
        final IMultiReportOutput output;
        output = new FileMultiReportOutput(getReportDir().toFile());
        final HTMLFormatter formatter = new HTMLFormatter();
        return formatter.createVisitor(output);
    }

    protected ISourceFileLocator createSourceLocator() {
        return new ISourceFileLocator() {
            @Override
            public Reader getSourceFile(String packageName, String fileName) throws IOException {
                for (Path srcdir : src) {
                    Path f = srcdir.resolve(packageName).resolve(fileName);
                    if (Files.exists(f) && !Files.isDirectory(f) && Files.isReadable(f)) {
                        return Files.newBufferedReader(f, srcCharset);
                    }
                }
                return null; // not found
            }

            @Override
            public int getTabWidth() {
                return srcTabWidth;
            }
        };
    }

    protected void createReport(final IReportGroupVisitor visitor, final ISourceFileLocator sourceslocator,
                                final ExecutionDataStore executionDataStore) throws IOException {
        final IBundleCoverage bundle = createBundle(executionDataStore);
        visitor.visitBundle(bundle, sourceslocator);
    }

    protected IBundleCoverage createBundle(final ExecutionDataStore executionDataStore) throws IOException {
        final CoverageBuilder builder = new CoverageBuilder();
        final Analyzer analyzer = new Analyzer(executionDataStore, builder);
        for (Path p : classes) {
            analyzer.analyzeAll(p.toFile());
        }
        final IBundleCoverage bundle = builder.getBundle(description.getDisplayName());
        logBundleInfo(bundle, builder.getNoMatchClasses());
        return bundle;
    }

    protected void logBundleInfo(final IBundleCoverage bundle, final Collection<IClassCoverage> nomatch) {
        log(format("Writing bundle '%s' with %s classes", bundle.getName(),
                   Integer.valueOf(bundle.getClassCounter().getTotalCount())));
        if (!nomatch.isEmpty()) {
            // FIXME warning
            log(format("Classes in bundle '%s' do no match with execution data. " +
                       "For report generation the same class files must be used as at runtime.", bundle.getName()));
            for (final IClassCoverage c : nomatch) {
                // FIXME warning
                log(format("Execution data for class %s does not match.", c.getName()));
            }
        }
    }

    protected String format(String s, Object... vals) {
        return String.format(s, vals);
    }

    protected void log(String s) {
        System.out.println(s);
    }

    protected Path getReportDir() throws IOException {
        return outputdir.resolve(getDecriptionFileName()).normalize().toAbsolutePath();
    }

    protected String getDecriptionFileName() {
        StringBuilder filename = new StringBuilder(description.getClassName());
        if (description.getMethodName() != null) {
            filename.append("-").append(description.getMethodName());
        }
        return filename.toString();
    }

    public static class Builder {
        private List<Path> classes = DFLT_PATH;
        private Path outputdir = Paths.get(".");
        private List<Path> src = DFLT_PATH;
        private Charset srcCharset = Charset.defaultCharset();
        private int srcTabWidth = 4;

        private static final List<Path> DFLT_PATH = Collections.singletonList(Paths.get("."));

        protected Builder() {
        }

        // can be invoked multiple times
        public Builder withClasses(final Path path) {
            if (classes == DFLT_PATH) {
                classes = new ArrayList<Path>();
            }
            classes.add(path);
            return this;
        }

        public Builder withOutputDir(final Path dir) {
            this.outputdir = dir;
            return this;
        }

        // can be invoked multiple times
        public Builder withSrc(final Path dir) {
            if (src == DFLT_PATH) {
                src = new ArrayList<Path>();
            }
            src.add(dir);
            return this;
        }

        public Builder withSrcCharset(final Charset charset) {
            this.srcCharset = charset;
            return this;
        }

        public Builder withSrcCharset(final String charset) {
            this.srcCharset = Charset.forName(charset);
            return this;
        }

        public Builder withSrcTabWidth(final int spaces) {
            this.srcTabWidth = spaces;
            return this;
        }

        public JacocoReporter build() {
            return new JacocoReporter(classes, outputdir, src, srcCharset, srcTabWidth);
        }
    }
}

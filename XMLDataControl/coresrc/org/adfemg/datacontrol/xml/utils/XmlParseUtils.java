package org.adfemg.datacontrol.xml.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.adf.model.adapter.AdapterException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * Utility methods for parsing and serializing XML.
 */
public final class XmlParseUtils {

    /**
     * Private constructor to prevent instances being created.
     */
    private XmlParseUtils() {
    }

    /**
     * Serialize a XML node to a string so it can be printed or persisted in another way.
     * @param node XML node to serialize
     * @return String containing the XML of the given node and all its children, if node is a document this will
     * also include the xml-declaration, otherwise not.
     */
    public static String nodeToString(final Node node) {
        return nodeToString(node, XmlFactory.newNonIndentingTransformer());
    }

    /**
     * Serialize a XML node to an indented (pretty printed) string so it can be printed or persisted in another way.
     * @param node XML node to serialize
     * @return String containing the XML of the given node and all its children, if node is a document this will
     * also include the xml-declaration, otherwise not.
     */
    public static String nodeToIndentedString(final Node node) {
        if (node == null) {
            return null;
        }
        // indentation with XSLT Transformer only works properly if source node has no whitespace or when
        // schema validation is turned on. Since we don't know the schema for given node, we strip all whitespace
        Node stripped = DomUtils.stripWhitespace(node);
        return nodeToString(stripped, XmlFactory.newIndentingTransformer());
    }

    private static String nodeToString(final Node node, final Transformer transformer) {
        if (node == null) {
            return null;
        }
        if (Node.DOCUMENT_NODE != node.getNodeType()) {
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        }
        StringWriter sw = new StringWriter(2048);
        try {
            transformer.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException e) {
            throw new AdapterException(e);
        }
        return sw.toString();
    }

    /**
     * Parse a given string with a namespace aware XML parser and return the resulting XML document.
     * <p><b>warning:</b> This should not be used to programmatically build XML from java code by first constructing
     * a string and then parsing it with this method. That can lead to all sorts of issues when the string does
     * not contain properly escaped characters or other XML specific challenges. This method is only intended for
     * parsing strings that are retrieved from an external source and are known to be well formatted xml.
     * If you want to build XML programmatically, start with XmlFactory.newDocumentElement or XmlFactory.newDocument
     * and build children with DomUtils.addChildElement
     * @param text xml string to parse
     * @return parsed xml document. if this was only to parse a single element simply look at
     * document.getDocumentElement
     * @throws SAXException when given string could not be parsed to xml, for example when it contains
     * invalid XML.
     * @see XmlFactory#newDocumentElement
     * @see XmlFactory#newDocument
     * @see DomUtils#addChildElement
     */
    public static Document parse(final String text) throws SAXException {
        try {
            return XmlFactory.newDocumentBuilder().parse(new InputSource(new StringReader(text)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

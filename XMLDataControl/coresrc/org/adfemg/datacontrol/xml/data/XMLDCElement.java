package org.adfemg.datacontrol.xml.data;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.model.adapter.dataformat.AttributeDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.AttributeDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.LeafNodeType;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.ha.SerializableUserData;
import org.adfemg.datacontrol.xml.handler.HandlerRegistry;
import org.adfemg.datacontrol.xml.handler.InsteadGetHandler;
import org.adfemg.datacontrol.xml.handler.InsteadPutHandler;
import org.adfemg.datacontrol.xml.handler.OperationHandler;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.ChangeTrackingUtils;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Wrapper for a XML Element to present it self as defined by a {@link StructureDefinition}.
 */
public class XMLDCElement implements XMLDCAccessorTarget, Map<String, Object> {

    private static final ADFLogger logger = ADFLogger.createADFLogger(XMLDCElement.class);

    // constructor args
    private final DataControl dc;
    private final StructureDefinition structDef;
    private final Element element;

    // internal properties (can also be used by customizers)
    Map<String, Object> properties;

    // based on StructureDefinition
    private final LinkedHashMap<String, Attribute> attributes = new LinkedHashMap<String, Attribute>();
    private final LinkedHashMap<String, Accessor> accessors = new LinkedHashMap<String, Accessor>();
    private final HandlerRegistry handlers;

    //////////////////// Constructor ////////////////////

    /**
     * Constructor.
     * @param dc DataControl  Instance which this collection is part of.
     * @param structDef StructureDefinition that describes how this element should behave
     *            at runtime and what structure the DataControl has.
     * @param element XML Element that returns the runtime information for this DataControl.
     */
    public XMLDCElement(final DataControl dc, final StructureDefinition structDef, final Element element) {
        this.dc = dc;
        this.structDef = structDef;
        this.element = element;
        this.handlers = new HandlerRegistry((StructureDef) structDef);

        // wrap attributes and accessors with their own class for lazy resolving
        for (Iterator iter = structDef.getAttributeDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof AttributeDef) {
                AttributeDefinition attrDef = (AttributeDefinition) obj;
                attributes.put(attrDef.getName(), new Attribute(attrDef));
            }
        }
        for (Iterator iter = structDef.getAccessorDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof AccessorDefinition) {
                AccessorDefinition acc = (AccessorDefinition) obj;
                accessors.put(acc.getName(), new Accessor(acc));
            }
        }
    }

    //////////////////// java.util.Map implementation ////////////////////

    /**
     * Returns the amount of defined attributes and accessors in this element.
     * @return the amount of defined attributes and accessors in the
     *         StructureDefinition. These don't have to contain a value.
     */
    @Override
    public int size() {
        return attributes.size() + accessors.size();
    }

    /**
     * @return {@code true} when the StructureDefinition has no attribute or
     *         accessor, else will return {@code false}.
     */
    @Override
    public boolean isEmpty() {
        return attributes.isEmpty() && accessors.isEmpty();
    }

    /**
     * @param key Name of the attribute or accessor.
     * @return {@code true} if there is a definition for a attribute or
     *         accessor with this name, even if the value inside is {@code null}.
     * @see #containsAttribute
     * @see #containsAccessor
     */
    @Override
    public boolean containsKey(final Object key) {
        return attributes.containsKey(key) || accessors.containsKey(key);
    }

    /**
     * This method is not implemented because it would require the values of all the
     * attributes and accessors to be desided.
     * This has a relatively high cost, while expected is that the {@code containsValue}
     * isn't needed.
     * @param value nvt
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public boolean containsValue(final Object value) {
        throw new UnsupportedOperationException("XMLDCElement.containsValue is not supported");
    }

    /**
     * Gets the value of an attribute or accessor, or {@code null} if not found.
     *
     * @param key The name of the attribute or the accessor.
     * @return In case of an attribute, this will be the value of the java object
     * as specified by the mapping in the {@link org.adfemg.datacontrol.xml.provider.typemap.TypeMapper}.
     * In case of an accessor, the {@link XMLDCAccessorTarget} to which
     * this accessor points. For accessors pointing to a collection this will be
     * a {@link XMLDCCollection}, while non-collection accesors will return a
     * {@link XMLDCElement}.
     */
    @Override
    @SuppressWarnings("oracle.jdeveloper.java.insufficient-catch-block")
    public Object get(final Object key) {
        // EL expressions like to invoke Map.get('xxx') before getXxx() so try that first
        // incase we are invoking something like thing.elementAsString which should map to
        // XMLDCElement.getElementAsString and not XMLDCElement.get('elementAsString')
        if (key instanceof String && ((String) key).length() >= 1) {
            try {
                String propName = (String) key;
                Method method =
                    getClass().getMethod("get" + propName.substring(0, 1).toUpperCase() + propName.substring(1));
                return method.invoke(this);
            } catch (NoSuchMethodException e) {
                // no getter method, just continue for normal processing
            } catch (Exception e) {
                throw new AdapterException(e);
            }
        }

        if (attributes.containsKey(key)) {
            Attribute dcattr = attributes.get(key);
            String attrName = dcattr.getDefinition().getName();
            if (handlers.hasHandler(InsteadGetHandler.class, attrName)) {
                return handlers.invokeInsteadGet(this, attrName);
            }
            if (dcattr.javaValue == null) {
                Node node = resolveAttribute(dcattr);
                if (node != null && node.getNodeType() == Node.ELEMENT_NODE &&
                    org.w3c.dom.Element.class.getName().equals(dcattr.def.getJavaTypeString())) {
                    /* special case where XML schema has a xsd:anyType and we expose the raw Element from the source
                     * clone the source node before returning it to prevent unexpected modifications to the node.
                     * relies on StructureProvider to have exposed an xsd:anyType as org.w3c.dom.Element as this is
                     * not supported by TypeMapper */
                    dcattr.javaValue = node.cloneNode(true);
                } else if (node != null) {
                    String nodetext = node.getTextContent();
                    // treat empty string as null for when using changeTracking which keeps the
                    // attribute or simple-element in the DOM with empty content
                    dcattr.javaValue =
                        getTypeMapper().toJava(StringUtils.isEmpty(nodetext) ? null : nodetext, dcattr.getXmlType(),
                                               dcattr.def.getJavaTypeString());
                }
            }
            return dcattr.javaValue;
        } else if (accessors.containsKey(key)) {
            return resolveAccessor(accessors.get(key));
        } else {
            logger.warning("Unknown attribute or accessor: {0}", key);
            return null;
        }
    }

    /**
     * Changes the value of an attribute within this element.
     * We use the {@link org.adfemg.datacontrol.xml.provider.typemap.TypeMapper}.
     *
     * @param key The name of the attribute to put.
     * @param value The new value to put on the attribute.
     * @return The old value of the attribute, this can be {@code null}.
     * @throws IllegalArgumentException If the key is not the name of an attribute.
     */
    @Override
    public Object put(final String key, final Object value) {
        if (attributes.containsKey(key)) {
            // fire PrePut handlers (if any)
            final AttrChangeEvent<Object> attrChangeEvent = new AttrChangeEvent<Object>(get(key), value, key, this);
            handlers.invokePrePut(attrChangeEvent);

            // perform actual XML manipulation (or invoke InsteadPutHandler if present)
            Object oldVal = putInternal(key, value);

            // fire PostPut handlers (if any)
            if (handlers.hasHandler(PostPutHandler.class, key)) {
                handlers.invokePostPut(attrChangeEvent, key);
            }

            return oldVal;
        } else if (accessors.containsKey(key)) {
            Accessor accessor = accessors.get(key);
            if (accessor.def.isCollection()) { // An accessor to an collection will always contain a (possible empty) collection.
                throw new IllegalArgumentException("kan via put geen collectie-accessor maken");
            }
            XMLDCElement oldTarget = (XMLDCElement) get(key); // Can not be an collection.
            if (oldTarget != null) { // Found existing element.
                if (value != null) {
                    throw new IllegalArgumentException("Can not override the accessor through put, only remove.");
                } else { // Remove an existing element.
                    Element removeElem = oldTarget.element;
                    removeElem.getParentNode().removeChild(removeElem);
                    accessor.reset();
                    return oldTarget;
                }
            }
            throw new IllegalArgumentException("Can not create an accessor through put.");
        } else {
            throw new IllegalArgumentException("\"" + key + "\" is not an attribute or accessor in " + this);
        }
    }

    /**
     * Changes attribute value without calling any PrePut or PostPut handlers
     * thereby not giving handlers an opportunity to mark/detect the attribute
     * as changed. Is invoked from {@link #put} but can also be invoked by
     * clients when they want to set a value without triggering normal pre- and
     * post-processing (for example setting a default value during object
     * creation)
     * @param key the name of the attribute.
     * @param value the new value of the attribute.
     * @return the old value, this can be {@code null}.
     */
    public Object putInternal(final String key, final Object value) {
        if (!attributes.containsKey(key)) {
            throw new IllegalArgumentException("attribute not found " + key);
        }
        final Attribute dcattr = attributes.get(key);
        final Object oldVal = get(key);

        // fire InsteadPutHandler (if present) or perform normal processing
        if (handlers.hasHandler(InsteadPutHandler.class, key)) {
            final AttrChangeEvent<Object> attrChangeEvent = new AttrChangeEvent<Object>(oldVal, value, key, this);
            handlers.invokeInsteadPut(attrChangeEvent, key);
        } else {
            final String newXmlVal = getTypeMapper().toXml(value, dcattr.getXmlType());
            final boolean changeTracking = isChangeTracking();
            Node existingNode = resolveAttribute(dcattr);
            dcattr.reset(); // reset so next fetch will re-resolve
            // TODO: not sure if this works with a XMLElement with attributes AND SimpleContent
            // this is a special case as we cannot remove the element when clearing the SimpleContent when other
            // attributes exists.
            // Also setting or clearing the special "_value" attribute shouldn't end up as <_value> in XML
            if (changeTracking && existingNode != null) {
                // when node already exists, store its existing value in a processing instruction
                ChangeTrackingUtils.markChanging(existingNode);
            }
            Node newNode;
            if (dcattr.getLeafNodeType() == LeafNodeType.ATTRIBUTE) {
                newNode = putInternalXmlAttribute(dcattr, newXmlVal, (Attr) existingNode);
            } else if (dcattr.getLeafNodeType() == LeafNodeType.ELEMENT) {
                newNode = putInternalXmlElement(dcattr, newXmlVal, (Element) existingNode);
            } else {
                throw new IllegalStateException("unsupported leaf node type: " + dcattr.getLeafNodeType());
            }
            if (changeTracking && existingNode == null) {
                // when a new node was created, mark it as new with a processing instruction
                ChangeTrackingUtils.markNewTextContent(newNode);
            }
        }
        return oldVal;
    }

    /**
     * putInternal implementation for LeafNodeType.ATTRIBUTE. Is invoked from {@link #putInternal} and assumed the
     * change-tracking itself is handled by the calling putInternal.
     * @param dcattr datacontrol attribute being changed of type LeafNodeType.ATTRIBUTE
     * @param newXmlVal new value for the datacontrol attribute and thus xml attribute
     * @param existingXmlAttr existing xml attribute, which might be {@code null} if the attribute has no value in the
     * document
     * @return changed or created xml attribute node will may be {@code null} when setting the attribute value to
     * {@code null} or empty string as this removes the xml attribute from the document
     */
    private Attr putInternalXmlAttribute(final Attribute dcattr, final String newXmlVal, final Attr existingXmlAttr) {
        if (StringUtils.isEmpty(newXmlVal)) {
            if (existingXmlAttr != null) {
                if (!isChangeTracking()) {
                    existingXmlAttr.getOwnerElement().removeAttributeNode(existingXmlAttr);
                    return null;
                } else {
                    existingXmlAttr.setValue("");
                    return existingXmlAttr;
                }
            } else {
                return null;
            }
        } else {
            if (existingXmlAttr != null) {
                existingXmlAttr.setValue(newXmlVal);
                return existingXmlAttr;
            } else {
                element.setAttributeNS(dcattr.getXmlNamespaceUri(), dcattr.getXmlName(), newXmlVal);
                return element.getAttributeNodeNS(dcattr.getXmlNamespaceUri(), dcattr.getXmlName());
            }
        }
    }

    /**
     * putInternal implementation for LeafNodeType.ELEMENT. Is invoked from {@link #putInternal} and assumed the
     * change-tracking itself is handled by the calling putInternal.
     * @param dcattr datacontrol attribute being changed of type LeafNodeType.ELEMENT
     * @param newXmlVal new value for the datacontrol attribute and thus xml simpletype element
     * @param existingXmlElement existing xml element, which might be {@code null} if the element does not exist in the
     * document
     * @return changed or created xml element node will may be {@code null} when setting the value to
     * {@code null} or empty string while change-tracking is disabled as this removes the xml attribute from the
     * document
     */
    private Element putInternalXmlElement(final Attribute dcattr, final String newXmlVal,
                                          final Element existingXmlElement) {
        if (StringUtils.isEmpty(newXmlVal)) {
            if (existingXmlElement != null) {
                if (!isChangeTracking()) {
                    existingXmlElement.getParentNode().removeChild(existingXmlElement);
                    return null;
                } else {
                    DomUtils.setTextContent(existingXmlElement, "");
                    return existingXmlElement;
                }
            } else {
                return null;
            }
        } else {
            if (existingXmlElement != null) {
                DomUtils.setTextContent(existingXmlElement, newXmlVal);
                return existingXmlElement;
            } else {
                return createNewElement(dcattr, newXmlVal);
            }
        }
    }

    /**
     * Creating a new Element for the attribute and its new value.
     * We create a new element with the correct name & namespace and set the
     * new value based on the input parameter.
     *
     * We look for the nextSibling Node and insert the Element before this Sibling.
     *
     * @param dcattr the attribute it concerns.
     * @param newXmlVal the new value the attribute gets.
     * @return the created Element.
     */
    private Element createNewElement(Attribute dcattr, String newXmlVal) {
        Element newElem = element.getOwnerDocument().createElementNS(dcattr.getXmlNamespaceUri(), dcattr.getXmlName());
        newElem.setTextContent(newXmlVal);
        // Find the next (non empty) child element within parent
        boolean foundInsertingElem = false;
        Node nextSibling = null;
        for (Attribute otherDcAttr : attributes.values()) {
            if (foundInsertingElem && LeafNodeType.ELEMENT.equals(otherDcAttr.getLeafNodeType()) &&
                resolveAttribute(otherDcAttr) != null) { // Found next filled element
                nextSibling = otherDcAttr.node;
                break;
            }
            foundInsertingElem = foundInsertingElem || (otherDcAttr == dcattr);
        }
        element.insertBefore(newElem, nextSibling); // if nextSibling==null will be added at the end.
        return newElem;
    }

    /**
     * Changes the value of an attribute or accessor within the element to {@code null}.
     *
     * @param key Name of the attribute or accessor to remove.
     * @return the previous/old value of the attribute or accessor.
     */
    @Override
    public Object remove(final Object key) {
        throw new UnsupportedOperationException("XMLDCElement.remove");
    }

    /**
     * Copies all the values from the given map to attributes or accessors of this map
     * by calling the {@link #put} for every entry in the given map.
     *
     * @param map the mappings that need to be copied to the map.
     */
    @Override
    public void putAll(final Map<? extends String, ? extends Object> map) {
        for (Map.Entry<? extends String, ? extends Object> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Not implemented.
     * @throws UnsupportedOperationException
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("XMLDCElement.clear not implemented");
    }

    /**
     * Gets a {@link Set} off all the attribute and accessor keys in this element definition.
     *
     * @return A unmodifiableSet of the names of attributes and accessors.
     */
    @Override
    public Set<String> keySet() {
        Set<String> keys = new HashSet<String>(attributes.keySet());
        keys.addAll(accessors.keySet());
        return Collections.unmodifiableSet(keys);
    }

    /**
     * Not implemented because this would require all the values of all the attributes
     * and all accessors to be determined, this has a relatively high cost, while you
     * expect that this is not needed.
     *
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public Collection<Object> values() {
        throw new UnsupportedOperationException("XMLDCElement.values is not supported. ");
    }

    /**
     * Not implemented because this would require all the values of all the attributes
     * and all accessors to be determined, this has a relatively high cost, while you
     * expect that this is not needed.
     *
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public Set<Map.Entry<String, Object>> entrySet() {
        throw new UnsupportedOperationException("XMLDCElement.entrySet is not supported. ");
    }

    //////////////////// geef inzage of een attribuut of accessor bestaat ////////////////////

    /**
     * Returns {@code true} if the StructureDefinition has an attribute with
     * the given name.
     *
     * @param key The name of the attribute.
     * @return Returns {@code true} if the StructureDefinition has an attribute with
     *                the given name, this can be the value {@code null}.
     * @see #containsKey
     */
    public boolean containsAttribute(final Object key) {
        return attributes.containsKey(key);
    }

    /**
     * Returns {@code true} if the StructureDefinition has an accessor with
     * the given name.
     * @param key The name of the accessor.
     * @return Returns {@code true} if the StructureDefinition has an accessor with
     *                the given name, this can be the value {@code null}.
     * @see #containsKey
     */
    public boolean containsAccessor(final Object key) {
        return accessors.containsKey(key);
    }

    protected final Accessor getAccessor(final Object name) {
        return accessors.get(name);
    }

    public boolean isCollection(final Object accessorName) {
        Accessor acc = accessors.get(accessorName);
        return (acc != null && acc.def.isCollection());
    }

    //////////////////// lazy resolving of attribute or accessor ////////////////////

    /**
     * Gets the node for a given attribute.
     * The response will be put into the {@code attribute} itself, so only the
     * first time we determine the XML Node and after that we can return the same Node.
     *
     * @param dcattr Attribute from the XMLDCElement that contains an AttributeDef.
     * @return XML Element or Attribute XML node the given {@code Attribute}
     *         points to or {@code null} if there is no such node.
     */
    protected Node resolveAttribute(final Attribute dcattr) {
        if (dcattr.node == null) { // attribute.node doesn't exist so determine it.
            if (LeafNodeType.SCALAR_COLLECTION_ELEMENT.equals(dcattr.getLeafNodeType())) { // This is an element withe a SimpleType in a collection.
                // In this case there is a "fake" attribute to get the value.
                dcattr.node = element;
            } else {
                dcattr.node =
                    LeafNodeType.ATTRIBUTE.equals(dcattr.getLeafNodeType()) ?
                    element.getAttributeNodeNS(dcattr.getXmlNamespaceUri(), dcattr.getXmlName()) :
                    DomUtils.findFirstChildElement(element, dcattr.getXmlNamespaceUri(), dcattr.getXmlName());
                if (dcattr.node == null) {
                    dcattr.node = XMLDCElement.Attribute.NOT_FOUND;
                }
            }
        }
        return dcattr.node == XMLDCElement.Attribute.NOT_FOUND ? null : dcattr.node;
    }

    /**
     * Gets or creates the XMLDCAccessorTarget for a given accessor.
     * The response will be put into the {@code accessor} itself, so only the
     * first time we determine the XMLDCAccessorTarget and after that we can
     * return the same XMLDCAccessorTarget.
     *
     * @param accessor Accessor from the XMLDCElement that contains an AccessorDef.
     * @return In case of an accossor to a collection, a (possible empty) XMLDCCollection
     *         In case of a singular an instance of the XMLDCElement if the child
     *         object exist, if the child object doesn't exist {@code null}.
     */
    protected XMLDCAccessorTarget resolveAccessor(final Accessor accessor) {
        if (accessor.target == null) {
            AccessorDefinition accessorDef = accessor.def;
            if (accessorDef.isCollection()) { // accessor is a collection
                accessor.target = new XMLDCCollection(dc, accessorDef, element);
            } else { // accessor to a singular object
                Node childNode =
                    DomUtils.findFirstChildElement(element, accessor.getXmlNamespaceUri(), accessor.getXmlName());
                if (childNode != null) {
                    accessor.target = new XMLDCElement(dc, accessorDef.getStructure(), (Element) childNode);
                } else {
                    accessor.target = XMLDCElement.Accessor.NOT_FOUND;
                }
            }
        }
        return accessor.target == XMLDCElement.Accessor.NOT_FOUND ? null : accessor.target;
    }

    public XMLDCAccessorTarget createChild(final String accessorName) {
        Accessor accessor = accessors.get(accessorName);
        if (accessor == null) {
            throw new IllegalArgumentException("accessor \"" + accessorName + "\" does not exist in " + this);
        }
        Element newElem =
            element.getOwnerDocument().createElementNS(accessor.getXmlNamespaceUri(), accessor.getXmlName());
        Node nextSibling =
            findInsertBeforeSibling(accessorName); // Find the first (non empty) child element within the parent
        element.insertBefore(newElem, nextSibling); // if nextSibling==null we add it to the end.
        accessor.reset();
        return resolveAccessor(accessor);
    }

    /**
     * Find the child node that should be used on element.insertBefore
     * @param insertingAccessorName name of the accessor (child) we want to insert
     * @return first sibling after the position where we need to add the child
     *         or null if none was found and the new child should be inserted as last child
     */
    public Node findInsertBeforeSibling(final String insertingAccessorName) {
        Accessor insAcc = accessors.get(insertingAccessorName);
        QName insName = new QName(insAcc.getXmlNamespaceUri(), insAcc.getXmlName());
        boolean pastInserting = false;

        // get ordered list of possible child elements so we know position where to add children
        @SuppressWarnings("unchecked")
        List<QName> childNames =
            (List<QName>) getDefinition().getProperty(DataControlDefinition.STRUCTPROP_CHILD_ELEMS);
        if (childNames == null) {
            childNames = Collections.emptyList();
        }

        for (QName child : childNames) {
            if (!pastInserting) {
                pastInserting = (insName.equals(child));
                continue;
            }
            // Iteration is passed the element 'to add', so we look if this element exists
            Element elem = DomUtils.findFirstChildElement(element, child.getNamespaceURI(), child.getLocalPart());
            if (elem != null) { // child-element found, this is the insert-before-point
                return elem;
            }
        }
        return null; // nothing found, return null so the insert will be at the end.
    }

    @Override
    public String toString() {
        return new StringBuilder().append(getClass().getSimpleName()).append("[element:").append(element == null ?
                                                                                                 null :
                                                                                                 element.getNodeName()).append(",dc:").append(dc).append("]").toString();
    }

    public Element getElement() {
        return element;
    }

    /**
     * Get the wrapped XML element as String. Can be useful in debugging to get the actual XML
     * content in a human readable form.
     * <p><b>Example:</b> Can be accessed from EL using
     * {@code #{bindings.Employees.currentRow.dataProvider.elementAsString}}
     * @return wrapped XML element as String
     */
    public String getElementAsString() {
        return XmlParseUtils.nodeToIndentedString(element);
    }

    //////////////////// method support ////////////////////

    public boolean hasMethod(String methodName, Map arguments) {
        return findOperation(methodName, arguments) != null;
    }

    public Object invokeMethod(String methodName, Map arguments) {
        return findOperation(methodName, arguments).invokeOperation(this, arguments);
    }

    private OperationHandler findOperation(String methodName, Map arguments) {
        for (OperationHandler operation : handlers.getHandlers(OperationHandler.class)) {
            if (operation.handlesOperation(methodName, arguments)) {
                return operation;
            }
        }
        return null;
    }

    public DataControl getDataControl() {
        return dc;
    }

    public StructureDefinition getDefinition() {
        return structDef;
    }

    /**
     * Returns a (mutable) map of implementation properties for this XMLDCElement instance.
     * Can be used for implementation specific details like change tracking, transient field
     * values, etc. It is up to the caller to ensure the keys that are used within this map
     * are unique among the different consumers that want to retain state in the properties.
     * @return Mutable Map, never {@code null}
     */
    public Map<String, Serializable> getProperties() {
        return SerializableUserData.forNode(getElement());
    }

    protected TypeMapper getTypeMapper() {
        return getDCDefinitionNode().getProviderInstance(TypeMapper.class);
    }

    /**
     * Method to determine if change-tracking is enabled for the corresponding datacontrol.
     * @return value of the change-tracking attribute in the DataControls.dcx configuration or false if this wasn't
     * specified.
     */
    protected boolean isChangeTracking() {
        return getDCDefinitionNode().isChangeTracking();
    }

    /**
     * Determines if change-tracking feature is enabled and the given attribute has changed its value through
     * {@link #put} during its lifetime.
     * @param attrName name of the attribute to check for changes
     * @return {@code true} if change-tracking is enabled for this datacontrol and the attribute value has
     * changed since its initial retrieval. Please note that the current value of the attribute might be the
     * samen as the original value and it could still be marked as changed.
     */
    public boolean isChanged(final String attrName) {
        if (!attributes.containsKey(attrName) || !isChangeTracking()) {
            return false;
        }
        final Attribute dcattr = attributes.get(attrName);
        final Node xmlnode = resolveAttribute(dcattr);
        return xmlnode == null ? false : ChangeTrackingUtils.isChanged(xmlnode);
    }

    public Object getOldValue(final String attrName) {
        if (!attributes.containsKey(attrName) || !isChangeTracking()) {
            return null;
        }
        final Attribute dcattr = attributes.get(attrName);
        final Node xmlnode = resolveAttribute(dcattr);
        String oldText = xmlnode == null ? null : ChangeTrackingUtils.getOldValue(xmlnode);
        return getTypeMapper().toJava(StringUtils.isEmpty(oldText) ? null : oldText, dcattr.getXmlType(),
                                      dcattr.def.getJavaTypeString());
    }

    /**
     * Determines if this XMLDCElement has been created after the initial data "fetch".
     * @return {@code true} if this XMLDCElement was created through the binding layer, otherwise {@code false}
     */
    public boolean isInserted() {
        return isChangeTracking() && ChangeTrackingUtils.isInserted(element);
    }

    /**
     * Determines if this XMLDCElement has been deleted from its XMLDCCollection after the initial data "fetch".
     * This means this particulat XMLDCElement can no longer be retrieved from its original XMLDCCollection but
     * client code might still have a reference to it.
     * @return {@code true} if this XMLDCElement was deleted through the binding layer, otherwise {@code false}
     */
    public boolean isDeleted() {
        return isChangeTracking() && ChangeTrackingUtils.isDeleted(element);
    }

    /**
     * Finds the DataControlDefinitionNode that defines the datacontrol method this XMLDCElement resulted from.
     * This is the &lt;definition&gt; node in the DataControls.dcx file.
     * @return DataControlDefinitionNode
     */
    protected DataControlDefinitionNode getDCDefinitionNode() {
        return dc.getDCDefinition().findDefinitionNode(this.getDefinition());
    }

    /**
     * Class to wrap an AttributeDefinition that can be lazy resolved to a real XML node.
     */
    protected static class Attribute {
        // Special marker for not found attributes.
        // This is to destinct between null (non resolved) Nodes and not found (resolved) Nodes.
        static final Node NOT_FOUND = ClassUtils.emptyInterfaceInstance(Node.class);

        final AttributeDefinition def;
        Node node; // XMLNode to get the text from. Lazy cached.
        Object javaValue;

        Attribute(final AttributeDefinition def) {
            this.def = def;
        }

        /**
         * Reset the reference to the underlying XML NOde and all the derived values.
         */
        void reset() {
            this.node = null;
            this.javaValue = null;
        }

        public AttributeDefinition getDefinition() {
            return def;
        }

        QName getXmlType() {
            return (QName) def.getProperty(DataControlDefinition.ATTRPROP_TYPE);
        }

        LeafNodeType getLeafNodeType() {
            return (LeafNodeType) def.getProperty(DataControlDefinition.ATTRPROP_LEAFNODETYPE);
        }

        String getXmlName() {
            return (String) def.getProperty(DataControlDefinition.ATTRPROP_NAME);
        }

        String getXmlNamespaceUri() {
            return (String) def.getProperty(DataControlDefinition.ATTRPROP_NAMESPACE);
        }

        @Override
        public String toString() {
            return getClass().getName() + "[" + getDefinition().getFullName() + "]";
        }
    }

    /**
     * Class to wrap an AccessorDefinition that can be lazy resolved to a XMLDCAccessorTarget.
     */
    protected static class Accessor {
        // Special marker for not found attributes.
        // This is to destinct between null (non resolved) Nodes and not found (resolved) Nodes.
        static final XMLDCAccessorTarget NOT_FOUND = ClassUtils.emptyInterfaceInstance(XMLDCAccessorTarget.class);

        final AccessorDefinition def;
        XMLDCAccessorTarget target; // XMLDCCollection or XMLDCElement

        Accessor(final AccessorDefinition def) {
            this.def = def;
        }

        /**
         * Reset the reference to the underlying XML NOde and all the derived values.
         */
        void reset() {
            this.target = null;
        }

        public AccessorDefinition getDefinition() {
            return def;
        }

        String getXmlName() {
            return (String) def.getProperty(DataControlDefinition.ACCPROP_NAME);
        }

        String getXmlNamespaceUri() {
            return (String) def.getProperty(DataControlDefinition.ACCPROP_NAMESPACE);
        }

        int getMinOccurs() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_MINOCCURS);
            return (obj instanceof Number ? ((Number) obj).intValue() : 1);
        }

        int getMaxOccurs() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_MAXOCCURS);
            return (obj instanceof Number ? ((Number) obj).intValue() : 1);
        }

        boolean isNillable() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_NILLABLE);
            return (obj instanceof Boolean ? ((Boolean) obj).booleanValue() : false);
        }

        @Override
        public String toString() {
            return getClass().getName() + "[" + getDefinition().getFullName() + "]";
        }
    }
}

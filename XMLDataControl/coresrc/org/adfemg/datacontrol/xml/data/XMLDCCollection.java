package org.adfemg.datacontrol.xml.data;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

import oracle.adf.model.adapter.dataformat.StructureDef;

import oracle.binding.meta.AccessorDefinition;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.handler.HandlerRegistry;
import org.adfemg.datacontrol.xml.utils.ChangeTrackingUtils;
import org.adfemg.datacontrol.xml.utils.DomUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Wrapper for a plural (maxOccurs&gt;1) XML Element to behave as defined
 * by an {@link AccessorDefinition}.
 * This wrapper presents itself as a list of {@link XMLDCElement}.
 */
public class XMLDCCollection extends AbstractList<XMLDCElement> implements XMLDCAccessorTarget, RandomAccess {

    private final List<XMLDCElement> elements;
    private final DataControl dc;
    private final AccessorDefinition accessorDef;
    private final HandlerRegistry handlers;
    private final String childElemNamespace;
    private final String childElemName;
    private final Element parentElement;

    //////////////////// Constructor ////////////////////

    /**
     * Default constructor.
     * @param dc DataControl Instance which this collection is part of.
     * @param accessorDef AccessorDefinition Prescribes how the collection has to
     *                    behave at runtime and which names the XML Elements have.
     * @param parentElement The parent XML element, we can find the children with
     *        {@link Element#getChildNodes()} and put them on hte collection.
     */
    XMLDCCollection(final DataControl dc, final AccessorDefinition accessorDef, final Element parentElement) {
        this.dc = dc;
        this.accessorDef = accessorDef;
        this.handlers = new HandlerRegistry((StructureDef) accessorDef.getStructure());
        this.parentElement = parentElement;
        this.childElemNamespace = (String) accessorDef.getProperty(DataControlDefinition.ACCPROP_NAMESPACE);
        this.childElemName = (String) accessorDef.getProperty(DataControlDefinition.ACCPROP_NAME);

        List<Element> children = DomUtils.findChildElements(parentElement, childElemNamespace, childElemName);
        this.elements = new ArrayList<XMLDCElement>(children.size());
        boolean changeTracking = isChangeTracking();
        for (Element el : children) {
            if (changeTracking && ChangeTrackingUtils.isDeleted(el)) {
                continue;
            }
            this.elements.add(new XMLDCElement(dc, accessorDef.getStructure(), el));
        }
    }

    //////////////////// java.util.AbstractList implementation ////////////////////

    /**
     * Get the XMLDCElement on the asked position.
     * @param index index of the element that needs to be returned.
     * @return XMLDCElement the element.
     * @throws IndexOutOfBoundsException Will be thrown for an invalid index.
     */
    @Override
    public XMLDCElement get(final int index) {
        return elements.get(index);
    }

    /**
     * Returns the amount of elements in this collection.
     * @return the amount of elements in this collection.
     */
    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public XMLDCElement remove(final int index) {
        XMLDCElement elemDc = elements.get(index);
        Element xmlElem = elemDc.getElement();
        if (isChangeTracking()) {
            // when changetracking: mark element as deleted, but keep it in the DOM
            ChangeTrackingUtils.markDeleted(xmlElem);
        } else {
            xmlElem.getParentNode().removeChild(xmlElem);
        }
        // Everything went ok, we can safely remove the element from the internal list.
        return elements.remove(index);
    }

    /**
     * Creates a new XMLDCElement "row" including the underlying XML element, adding this new element as a
     * child of the master xml element and invoking any @Created annotations on the XMLDCElement
     * Invoked from DataControl#createRowData when a new row is explicitly being created.
     * @param index The index where the new element should be placed in front of.
     *              Use -1 for adding the first element to a empty collection.
     * @return the XMLDCElement.
     * @throws ArrayIndexOutOfBoundsException when given index does not point to an existing row or is not -1 with
     * an empty collection
     */
    public XMLDCElement createElement(final int index, final XMLDCElement master) {
        // create XML element (without yet adding it to the XML document tree)
        Element xmlElem = parentElement.getOwnerDocument().createElementNS(childElemNamespace, childElemName);
        // Add processing instruction if changetracking is on.
        if (isChangeTracking()) {
            ChangeTrackingUtils.markInserted(xmlElem);
        }
        // create a XMLDCElement to wrap the xml element
        XMLDCElement elemDc = new XMLDCElement(dc, accessorDef.getStructure(), xmlElem);
        // add new xml element to the document tree and new XMLDCElement to this collection
        if (index >= 0 && index < size()) {
            // adding to an existing (non empty) collection
            Node nextSibling = get(index).getElement();
            parentElement.insertBefore(xmlElem, nextSibling); // nextSibling==null will insert at the end
            elements.add(index, elemDc);
        } else if (index == -1 && size() == 0) {
            // adding to empty collection, find sibling to insert new element before
            Node nextSibling = master.findInsertBeforeSibling(accessorDef.getName());
            parentElement.insertBefore(xmlElem, nextSibling); // nextSibling==null will insert at the end
            elements.add(elemDc);
        } else {
            throw new ArrayIndexOutOfBoundsException("invalid index " + index + " with size " + size());
        }
        // invoke any @Created annotations on the new XMLDCElement
        handlers.invokeCreated(elemDc);
        return elemDc;
    }

    /**
     * Method to determine if change-tracking is enabled for the corresponding datacontrol.
     * @return value of the change-tracking attribute in the DataControls.dcx configuration or false if this wasn't specified.
     */
    private boolean isChangeTracking() {
        return dc.getDCDefinition().findDefinitionNode(accessorDef.getStructure()).isChangeTracking();
    }

}
